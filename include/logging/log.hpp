#pragma once

#include "main/common.hpp"
#include "structures/stream.hpp"

namespace log {

const uint8_t LEVEL_MESSAGE = 0x0;
const uint8_t LEVEL_WARN = 0x1;
const uint8_t LEVEL_ERROR = 0x2;

/** A single log entry, consisting of a message and and a number of flags */
struct LogEntry {
  Utf8 message;
  uint8_t flag;
};

/** A logging stream
 *
 * Log entries are divided by \n. When one is encountered in a stream, a new log entry will be created and appended to
 * the entries list.
 */
class LogStream : public stream::Stream {
public:
  error_t write(const void *buff, size_t len, uint32_t flags, void *data, uint32_t *written);
};

/** The standard kernel logging stream */
extern LogStream stream;

/** All the kernel log entries, in order
 *
 * This list may grow (and reallocate) at any time, however existing entries are never deleted.
 */
extern vector<LogEntry> entries;
} // namespace log
