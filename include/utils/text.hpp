#pragma once
#include "main/common.hpp"

namespace utils_text {
/** Returns character boundries to split the text on to perform text wrapping.
 *
 * \n is handled correctly (will produce a break just before the \n), but words longer than a line are currently not.
 *
 * @param text The text to break into lines.
 * @param width The maximum width of a line.
 */
vector<size_t> ascii_break_into_lines(Utf8 text, size_t width);
} // namespace utils_text
