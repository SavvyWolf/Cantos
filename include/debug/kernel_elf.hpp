#pragma once

#include "main/common.hpp"
#include "structures/elf.hpp"

namespace kernel_elf {
extern elf::Header *kernel_elf;
void load_kernel_elf(uint32_t num, uint32_t size, addr_logical_t addr, uint32_t shndx);
} // namespace kernel_elf
