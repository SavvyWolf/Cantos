#ifndef __HPP_MEM_PAGES__
#define __HPP_MEM_PAGES__

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "main/common.hpp"
#include "main/loader_data.hpp"
#include "main/multiboot.hpp"

namespace page {
class Page {
public:
  uint32_t page_id;
  uint32_t consecutive;
  addr_phys_t mem_base_flags;
  Page *next;

  uint32_t count() const;
  Page *split(uint32_t count);
  addr_phys_t get_mem_base() const;
  addr_phys_t get_mem_limit() const;
  uint8_t get_flags() const;
};

const uint8_t FLAG_ALLOCATED = 0x01;
const uint8_t FLAG_KERNEL = 0x02;
const uint8_t FLAG_RESERVED = 0x04;
const uint8_t FLAG_NOLOCK = 0x08;

const uint32_t FREE_MASK = 0xfff;

const uint32_t PAGE_TABLE_SHIFT = 12;
const uint32_t PAGE_TABLE_MASK = 0x3ff;
const uint32_t PAGE_DIR_SHIFT = 22;

// 64b tables
namespace width64 {
typedef uint64_t page_table_entry_t;
struct page_table_t {
  page_table_entry_t entries[PAGE_TABLE_LENGTH_64];
};

typedef uint64_t page_dir_entry_t;
struct page_dir_t {
  page_dir_entry_t entries[PAGE_TABLE_LENGTH_64];
};

typedef uint64_t page_dir_pointer_entry_t;
struct page_dir_pointer_t {
  page_dir_pointer_entry_t entries[PAGE_TABLE_LENGTH_64];
};

typedef uint64_t pml4_entry_t;
struct page_pml4_t {
  pml4_entry_t entries[PAGE_TABLE_LENGTH_64];
};

struct logical_dir_t {
  Page *pages[PAGE_TABLE_LENGTH];
  page_table_t *tables[PAGE_TABLE_LENGTH_64];
};

struct logical_pdp_t {
  Page *pages[PAGE_TABLE_LENGTH];
  page_dir_pointer_t *tables[PAGE_TABLE_LENGTH_64];
  logical_dir_t *logicals[PAGE_TABLE_LENGTH_64];
};

struct logical_pml4_t {
  Page *pages[PAGE_TABLE_LENGTH_64];
  page_pml4_t *tables[PAGE_TABLE_LENGTH_64];
  logical_pdp_t *logicals[PAGE_TABLE_LENGTH_64];
};
} // namespace width64

// 32b Page table stuff
namespace width32 {
typedef uint32_t page_table_entry_t;
struct page_table_t {
  page_table_entry_t entries[PAGE_TABLE_LENGTH];
};

typedef uint32_t page_dir_entry_t;
struct page_dir_t {
  page_dir_entry_t entries[PAGE_TABLE_LENGTH];
};

struct logical_dir_t {
  Page *pages[PAGE_TABLE_LENGTH - (KERNEL_VM_PAGE_TABLES)];
  page_table_t *tables[PAGE_TABLE_LENGTH - (KERNEL_VM_PAGE_TABLES)];
};
} // namespace width32

#ifdef X86_64
using namespace width64;
#else
using namespace width32;
#endif

#define PAGE_TABLE_NOFLAGS(x) ((x) & ~PAGE_TABLE_FLAGMASK)
const uint32_t PAGE_TABLE_FLAGMASK = 0xfff;
const uint32_t PAGE_TABLE_PRESENT = 0x01;
const uint32_t PAGE_TABLE_RW = 0x02;
const uint32_t PAGE_TABLE_USER = 0x04;
const uint32_t PAGE_TABLE_WRITETHROUGH = 0x08;
const uint32_t PAGE_TABLE_CACHEDISABLE = 0x10;
const uint32_t PAGE_TABLE_ACCESSED = 0x20;
const uint32_t PAGE_TABLE_DIRTY = 0x40;
const uint32_t PAGE_TABLE_SIZE = 0x80;
const uint32_t PAGE_TABLE_GLOBAL = 0x100;

extern page_dir_t *page_dir;

void init(loader_data::LoaderData &ld);
Page *alloc(uint8_t flags, unsigned int count);
Page *alloc_nokmalloc(uint8_t flags, unsigned int count);
Page *create(uint32_t base, uint8_t flags, unsigned int count);
void free(Page *page);
void used(Page *page, bool lock = true);
void *kinstall(const Page *page, uint8_t page_flags);
void *kinstall_append(const Page *page, uint8_t page_flags,
                      bool lock = true); // Doesn't kmalloc, but doesn't reuse any existing memory
// either
void kuninstall(volatile void *base, const Page *page);

class PageRaii {
private:
  Page *page;
  void *installed;

public:
  PageRaii() : page(nullptr), installed(nullptr){};
  PageRaii(uint8_t flags, unsigned int count) : page(alloc(flags, count)), installed(nullptr) {}
  PageRaii(PageRaii &) = delete;
  PageRaii(PageRaii &&other) : page(other.page), installed(other.installed) {
    other.page = nullptr;
    other.installed = nullptr;
  }
  PageRaii(Page *page) : page(page) {}

  ~PageRaii() {
    if (installed) {
      kuninstall(installed, page);
    }
    free(page);
  }

  void install(uint8_t page_flags) {
    assert(!installed);
    installed = kinstall(page, page_flags);
  }
  void uninstall() {
    assert(installed);
    kuninstall(installed, page);
    installed = nullptr;
  }

  const Page *get_page() const {
    assert(page);
    return page;
  }
  Page *get_page() {
    assert(page);
    return page;
  }

  template <typename T> T *data() const { return reinterpret_cast<T *>(installed); }

  const Page &operator*() const { return *page; }
  const Page *operator->() const { return page; }

  PageRaii &operator=(PageRaii &&other) {
    page = other.page;
    installed = other.installed;
    other.page = nullptr;
    other.installed = nullptr;
    return *this;
  }
};
} // namespace page

#endif
