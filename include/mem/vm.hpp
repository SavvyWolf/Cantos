#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "mem/page.hpp"
#include "structures/list.hpp"
#include "structures/shared_ptr.hpp"

namespace object {
class Object;
class ObjectInMap;
} // namespace object

namespace vm {

template <uint32_t TableLength, typename RawTableType> class PageTable {
  page::PageRaii this_page;

  RawTableType *mount_page() {
    this_page.install(page::PAGE_TABLE_RW);
    return this_page.data<RawTableType>();
  }
  void unmount_page(RawTableType *data) {
    this_page.uninstall();
  }

public:
  PageTable() : this_page(page::FLAG_KERNEL, 1) {
    auto mnt = mount_page();
    for (uint32_t i = 0; i < TableLength; i++) {
      mnt->entries[i] = 0;
    }
    unmount_page(mnt);
  }
  void set(addr_logical_t logical, uint32_t physical, uint8_t page_flags) {
    physical &= ~page::PAGE_TABLE_FLAGMASK;
    page_flags &= page::PAGE_TABLE_FLAGMASK;
    uint32_t index = (logical / PAGE_SIZE) % TableLength;
    auto mnt = mount_page();
    mnt->entries[index] = physical | page_flags;
    unmount_page(mnt);
  }
  void set_raw_nochildren(addr_logical_t address, uint32_t value) {
    uint32_t index = (address / PAGE_SIZE) % TableLength;
    auto mnt = mount_page();
    mnt->entries[index] = value;
    unmount_page(mnt);
  }
  void clear(addr_logical_t address) {
    uint32_t index = (address / PAGE_SIZE) % TableLength;
    auto mnt = mount_page();
    mnt->entries[index] = 0;
    unmount_page(mnt);
  }
  addr_phys_t get_addr() const { return this_page->get_mem_base(); }
};


template <typename Child, uint32_t TableLength, uint64_t TableEntrySize, typename RawTableType> class PageIndirect {
  page::PageRaii this_page;
  Child *subtables[TableLength];

  RawTableType *mount_page() {
    this_page.install(page::PAGE_TABLE_RW);
    return this_page.data<RawTableType>();
  }
  void unmount_page(RawTableType *data) {
    this_page.uninstall();
  }
  void make_subtable(addr_logical_t index, RawTableType *table) {
    subtables[index] = new Child();
    table->entries[index] = subtables[index]->get_addr() | page::PAGE_TABLE_PRESENT;
  }

public:
  PageIndirect() : this_page(page::FLAG_KERNEL, 1), subtables() {
    auto mnt = mount_page();
    for (uint32_t i = 0; i < TableLength; i++) {
      mnt->entries[i] = 0;
    }
    unmount_page(mnt);
  }
  ~PageIndirect() {
    for (uint32_t i = 0; i < TableLength; i++) {
      if (subtables[i]) {
        delete subtables[i];
      }
    }
  }
  void set(addr_logical_t logical, addr_phys_t physical, uint8_t page_flags) {
    uint32_t index = (logical / TableEntrySize) % TableLength;
    auto mnt = mount_page();
    if (!subtables[index])
      make_subtable(index, mnt);
    subtables[index]->set(logical, physical, page_flags);
    unmount_page(mnt);
  }
  void set_raw_nochildren(addr_logical_t address, uinta_t value) {
    uint32_t index = (address / TableEntrySize) % TableLength;
    auto mnt = mount_page();
    mnt->entries[index] = value;
    unmount_page(mnt);
  }
  void clear(addr_logical_t address) {
    uint32_t index = (address / TableEntrySize) % TableLength;
    auto mnt = mount_page();
    if (!subtables[index])
      return;
    assert(subtables[index] && "Tried to clear from something with no subtables!?");
    subtables[index]->clear(address);
    unmount_page(mnt);
  }
  addr_phys_t get_addr() const { return this_page->get_mem_base(); }
};

typedef PageTable<PAGE_TABLE_LENGTH, page::width32::page_table_t> PageTable32;
typedef PageIndirect<PageTable32, PAGE_TABLE_LENGTH, PAGE_DIR_SIZE, page::width32::page_dir_t> PageDirectory32;

typedef PageTable<PAGE_TABLE_LENGTH_64, page::width64::page_table_t> PageTable64;
typedef PageIndirect<PageTable64, PAGE_TABLE_LENGTH_64, PAGE_DIR_SIZE_64, page::width64::page_dir_t> PageDirectory64;
typedef PageIndirect<PageDirectory64, PAGE_TABLE_LENGTH_64, PAGE_PDP_SIZE_64, page::width64::page_dir_pointer_t>
    PageDirectoryPointer64;
typedef PageIndirect<PageDirectoryPointer64, PAGE_TABLE_LENGTH_64, PAGE_PML4_SIZE_64, page::width64::page_pml4_t>
    PagePml464;

class BaseMap {
protected:
  list<unique_ptr<object::ObjectInMap>> objects_in_maps;
  uint32_t using_cpu = 0xffffffff;

  void invlpg(addr_logical_t addr);

public:
  uint32_t pid;
  uint32_t task_id;

  BaseMap(uint32_t pid, uint32_t task_id, bool kernel);
  virtual ~BaseMap();
  virtual void insert(int64_t addr, page::Page *page, uint8_t page_flags, uint32_t min, uint32_t max) = 0;
  virtual void clear(int64_t addr, uint32_t pages) = 0;
  bool resolve_fault(addr_logical_t addr);

  void add_object(const shared_ptr<object::Object> &object, uint32_t base, int64_t offset, uint32_t pages);
  void remove_object(const shared_ptr<object::Object> &object);
  void remove_object_at(const shared_ptr<object::Object> &object, uint32_t base);

  virtual void enter() = 0;
  void exit();
};

namespace width32 {
class Map : public BaseMap {
  PageDirectory32 directory;

public:
  Map(uint32_t pid, uint32_t task_id, bool kernel);
  ~Map();

  void insert(int64_t addr, page::Page *page, uint8_t page_flags, uint32_t min, uint32_t max);
  void clear(int64_t addr, uint32_t pages);
  void enter();
};
} // namespace width32

namespace width64 {
class Map : public BaseMap {
  PagePml464 pml4;

public:
  Map(uint32_t pid, uint32_t task_id, bool kernel);
  ~Map();

  void insert(int64_t addr, page::Page *page, uint8_t page_flags, uint32_t min, uint32_t max);
  void clear(int64_t addr, uint32_t pages);
  void enter();
};
} // namespace width64

#ifdef X86_64
using namespace width64;
#else
using namespace width32;
#endif
} // namespace vm
