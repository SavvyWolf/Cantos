#pragma once

#include "hw/pit.hpp"
#include "main/common.hpp"

namespace cmos {
const uint8_t DAYS_IN_MONTH[] = {0, 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const uint8_t UPDATE_MINUTES = 10; // Update the clock from CMOS every X minutes

struct Time {
  uint16_t subsecond;
  uint8_t second;
  uint8_t minute;
  uint8_t hour;
  uint8_t dom;
  uint8_t month;
  uint8_t year;
  uint8_t century;

  bool operator==(Time &other) const {
    return other.second == second && other.minute == minute && other.hour == hour && other.dom == dom &&
           other.month == month && other.year == year && other.century == century;
  }

  void operator=(const Time &other) volatile {
    if (this != &other) {
      second = other.second;
      minute = other.minute;
      hour = other.hour;
      dom = other.dom;
      month = other.month;
      year = other.year;
      century = other.century;
    }
  }

  int64_t epoch_time() const volatile;
  int64_t epoch_time_us() const volatile;
};

extern volatile Time utc;
void init();
void increment();
void sched_read_time();

uint8_t read_data(uint8_t reg);
void write_data(uint8_t reg, uint8_t data);
} // namespace cmos
