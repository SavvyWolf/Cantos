#pragma once

#include "fs/filesystem.hpp"
#include "main/asm_utils.hpp"
#include "main/common.hpp"
#include "task/task.hpp"

namespace storage_worker {

typedef uint32_t worker_id_t;
typedef uint32_t work_id_t;
typedef uint32_t work_slot_id_t;

worker_id_t generate_worker_id();
work_id_t generate_work_id();

enum class WorkOrderState : uint8_t { not_set, pending, in_progress, complete };

struct WorkOrder {
  work_id_t id;
  work_slot_id_t slot = -1;

  shared_ptr<task::Thread> task;

  worker_id_t worker;

  WorkOrderState state;

  // Flags
  uint8_t block : 1;
  uint8_t write : 1;

  page::PageRaii host;
  uint64_t device;
  uint64_t size;

  WorkOrder() : id(generate_work_id()), task(task::get_thread()), state(WorkOrderState::not_set), host() {}
};

class Worker {
  vector<WorkOrder *> pending;

public:
  worker_id_t id;
  bool need_update = false;

  void accept(WorkOrder *wo);
  void update();

  virtual bool dev_handle(WorkOrder *wo) = 0;
};

class WorkerBackedStorage : public filesystem::Storage {
private:
  worker_id_t worker;
  size_t offset;
  uint32_t flags;

public:
  WorkerBackedStorage(worker_id_t worker, size_t offset, uint32_t flags)
      : worker(worker), offset(offset), flags(flags){};
  Failable<page::PageRaii > read(addr_logical_t addr, uint32_t count) override;
};

void init();

void add_worker(Worker *worker);
void submit_work(WorkOrder *wo);

WorkOrder *get_work(work_slot_id_t slot_id);
void complete_work(work_slot_id_t slot_id);

void update();
} // namespace storage_worker
