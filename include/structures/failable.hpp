#ifndef _HPP_STRUCT_FAILABLE_
#define _HPP_STRUCT_FAILABLE_

#include <stddef.h>

#include "main/common.hpp"
#include "main/errno.h"

namespace failable {
namespace {
class empty {};
} // namespace

template <class T> class Failable {
public:
  error_t err;
  union {
    T val;
    empty e;
  };

  explicit constexpr Failable(error_t err) : err(err), e(empty()) {}
  constexpr Failable(T& val) : err(EOK), val(val) {}
  constexpr Failable(T&& val) : err(EOK), val(move(val)) {}
  constexpr Failable(Failable &other) : err(other.err), val(other.val) {}
  constexpr Failable(Failable &&other) : err(other.err), val(move(other.val)) {}

  ~Failable() {
    if (err == EOK) {
      val.~T();
    }
  }

  constexpr explicit operator bool() const { return err == EOK; }
};
} // namespace failable
#endif
