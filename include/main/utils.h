#pragma once

#include <stddef.h>

/** @file main/utils.hpp
 *
 * Contains a number of utility functions, stuff like memcpy and strlen
 */
#ifdef __cplusplus
extern "C" {
#endif
void *memcpy(void *destination, const void *source, size_t num);
size_t strlen(const char *str);
void *memset(void *ptr, int value, size_t num);
volatile void *memset_v(volatile void *ptr, int value, size_t num);
char *strcat(char *destination, const char *source);
char *strncat(char *destination, const char *source, size_t num);
int strcmp(const char *str1, const char *str2);
int strncmp(const char *str1, const char *str2, size_t num);
char *strcpy(char *destination, const char *source);
char *strncpy(char *destination, const char *source, size_t num);
int memcmp(const void *ptr1, const void *ptr2, size_t num);
int tolower(int c);
int toupper(int c);
int isprint(int c);
int isdigit(int c);
int isxdigit(int c);
int isspace(int c);
#ifdef __cplusplus
}
#endif
