#pragma once

#include "main/multiboot.hpp"
#include <stdint.h>

namespace loader_data {
struct LoaderData {
  multiboot::MultibootData multiboot_data;
  uint64_t pml4_page;
  uint64_t pdp_low_page;
  uint64_t pdp_high_page;
  uint64_t dir_high_page;

  uint64_t allocation_start;
  uint64_t upper_page_dir;
  uint64_t stack_page;
};

extern LoaderData passed_loader_data;
} // namespace loader_data
