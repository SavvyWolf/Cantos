#ifndef _H_MAIN_COMMON_
#define _H_MAIN_COMMON_

/** @file main/common.hpp
 *
 * Contains common definitions used by multiple files and which aren't specific to any one.
 *
 * Including common.hpp will also include the following headers (which should be seen as "always available"):
 *
 * * assert.hpp
 * * utils.h
 * * stdint.h
 * * asm_utils.hpp
 * * panic.hpp
 * * printk.hpp
 * * list.hpp
 * * vector.hpp
 * * mutex.hpp
 * * shared_ptr.hpp
 * * unique_ptr.hpp
 * * utf8.hpp
 * * errno.h
 * * failable.hpp
 * * cpp.hpp
 *
 * In addition, the following will be made available in the global scope, and thus won't require namespace scoping:
 *
 * * list_ns::list
 * * vector_ns::vector
 * * mutex::Mutex
 * * mutex::LockGuard
 * * mutex::CliLockGuard
 * * shared_ptr_ns::shared_ptr
 * * shared_ptr_ns::make_shared
 * * unique_ptr_ns::unique_ptr
 * * unique_ptr_ns::make_unique
 * * utf8::Utf8
 * * failable::Failable
 * * cpp::move
 * * cpp::forward
 * * cpp::nullptr_t
 */

#include <stdint.h>

/** Processor state, in the same format as pushed by the `PUSHAD` instruction.
 *
 * That is, `PUSHAD` before a function call will have a structure of this type as its argument.
 */
struct idt_proc_state_32_t {
  uint32_t reg_di;
  uint32_t reg_si;
  uint32_t reg_bp;
  uint32_t reg_sp;
  uint32_t reg_bx;
  uint32_t reg_dx;
  uint32_t reg_cx;
  uint32_t reg_ax;
};

struct idt_proc_state_64_t {
  uint64_t reg_bp;
  uint64_t reg_11;
  uint64_t reg_10;
  uint64_t reg_9;
  uint64_t reg_8;
  uint64_t reg_cx;
  uint64_t reg_dx;
  uint64_t reg_si;
  uint64_t reg_di;
  uint64_t reg_ax;
};

#if X86_64
typedef idt_proc_state_64_t idt_proc_state_t;
#else
typedef idt_proc_state_32_t idt_proc_state_t;
#endif

/** Variables of this type represent a physical address
 *
 * That is, an address in physical memory, ignoring the page table.
 *
 * This can be cast to and from a void * freely.
 */
typedef uintptr_t addr_phys_t;

/** Variables of this type represent a logical address
 *
 * That is, an address after paging has been applied.
 *
 * This can be cast to and from a void * freely.
 */
typedef uintptr_t addr_logical_t;

#define PAGE_SIZE 0x1000
#define KERNEL_VM_SIZE 0x40000000

// 64b stuff
#define PAGE_TABLE_LENGTH_64 0x200LL
#define PAGE_HIGH_MASK_64 0xffff000000000000
#define PAGE_DIR_SIZE_64 (PAGE_TABLE_LENGTH_64 * PAGE_SIZE)
#define PAGE_PDP_SIZE_64 (PAGE_TABLE_LENGTH_64 * PAGE_DIR_SIZE_64)
#define PAGE_PML4_SIZE_64 (PAGE_TABLE_LENGTH_64 * PAGE_PDP_SIZE_64)
#define TOTAL_VM_SIZE_64 (PAGE_TABLE_LENGTH_64 * PAGE_PML4_SIZE_64)
#define KERNEL_VM_SIZE_64 PAGE_DIR_SIZE_64
#define KERNEL_VM_PAGES_64 (KERNEL_VM_SIZE_64 / PAGE_SIZE)
#define KERNEL_VM_PAGE_TABLES_64 (KERNEL_VM_PAGES_64 / PAGE_TABLE_LENGTH_64)
#define KERNEL_VM_BASE_64 ((TOTAL_VM_SIZE_64 - PAGE_PDP_SIZE_64) | PAGE_HIGH_MASK_64) // Higher half kernel

// 32b stuff
#define PAGE_TABLE_LENGTH 0x400
#define PAGE_DIR_SIZE (PAGE_TABLE_LENGTH * PAGE_SIZE)
#define TOTAL_VM_SIZE 0x100000000
#define KERNEL_VM_PAGES (KERNEL_VM_SIZE / PAGE_SIZE)
#define KERNEL_VM_PAGE_TABLES (KERNEL_VM_PAGES / PAGE_TABLE_LENGTH)
#define KERNEL_VM_BASE (TOTAL_VM_SIZE - KERNEL_VM_SIZE) // Higher half kernel


// Cast to the low address of a variable
#define LOW(t, x) (*(t *)((addr_phys_t)&x - (addr_phys_t)KERNEL_VM_BASE))

#define MAX_CORES 32

#define GDT_SELECTOR(rpl, ti, index) ((rpl) | ((ti) << 2) | ((index) << 3))

template <class T> using callback_t = void (*)(T);

#if CHECK_IF
#include "main/panic.hpp"
#define CHECK_IF_SET                                                                                                   \
  {                                                                                                                    \
    uint32_t flags;                                                                                                    \
    asm("pushf; pop %0;" : "=r"(flags));                                                                               \
    if (!(flags & 0x200))                                                                                              \
      panic("IF not set in %s", __func__);                                                                             \
  }
#define CHECK_IF_CLR                                                                                                   \
  {                                                                                                                    \
    uint32_t flags;                                                                                                    \
    asm("pushf; pop %0;" : "=r"(flags));                                                                               \
    if (flags & 0x200)                                                                                                 \
      panic("IF not clear in %s", __func__);                                                                           \
  }
#else
#define CHECK_IF_SET
#define CHECK_IF_CLR
#endif

#if X86_64
typedef int64_t inta_t;
typedef uint64_t uinta_t;
#else
typedef int32_t inta_t;
typedef uint32_t uinta_t;
#endif

#include "debug/assert.hpp"
#include "main/cpp.hpp"
#include "main/utils.h"
using cpp::forward;
using cpp::move;
using cpp::nullptr_t;
#include "structures/shared_ptr.hpp"
using shared_ptr_ns::make_shared;
using shared_ptr_ns::shared_ptr;
#include "structures/unique_ptr.hpp"
using unique_ptr_ns::make_unique;
using unique_ptr_ns::unique_ptr;
#include "main/asm_utils.hpp"
#include "main/errno.h"
#include "main/panic.hpp"
#include "logging/printk.hpp"
#include "mem/kmem.hpp"
#include "structures/failable.hpp"
#include "structures/list.hpp"
#include "structures/mutex.hpp"
#include "structures/shared_ptr.hpp"
#include "structures/unique_ptr.hpp"
#include "structures/utf8.hpp"
#include "structures/vector.hpp"

using failable::Failable;
using list_ns::list;
using mutex::CliLockGuard;
using mutex::LockGuard;
using mutex::Mutex;
using utf8::Utf8;
using vector_ns::vector;

#endif
