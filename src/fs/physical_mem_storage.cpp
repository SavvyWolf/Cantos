#include <stdint.h>

#include "fs/physical_mem_storage.hpp"
#include "logging/printk.hpp"
#include "main/cpu.hpp"
#include "main/errno.h"
#include "structures/utf8.hpp"
#include "test/test.hpp"

namespace physical_mem_storage {
using namespace filesystem;

Failable<page::PageRaii> PhysicalMemStorage::read(addr_logical_t addr, uint32_t count) {
  page::Page *page = page::create(base + addr, flags, count);

  return page::PageRaii(page);
}
} // namespace physical_mem_storage
