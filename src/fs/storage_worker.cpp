#include <stdint.h>

#include "fs/storage_worker.hpp"

namespace storage_worker {

namespace {
worker_id_t worker_id_counter = 0;
work_id_t work_id_count = 0;

vector<Worker *> workers;
Mutex workers_mutex;

vector<WorkOrder *> orders;
Mutex orders_mutex;

task::wchan_t main_chan;
shared_ptr<task::Thread> main_thread;
void thread() {
  while (true) {
    for (auto w : workers) {
      if (w->need_update) {
        w->update();
        w->need_update = false;
      }
    }
    task::wait(main_chan);
  }
}

task::wchan_t io_chan;
} // namespace

worker_id_t generate_worker_id() { return worker_id_counter++; }
work_id_t generate_work_id() { return work_id_count++; }

void init() {
  main_chan = task::new_wchan("storage_worker::main_chan");
  io_chan = task::new_wchan("storage_worker::io_chan");
  main_thread = task::kernel_process->new_thread((addr_logical_t)&thread);
}

void add_worker(Worker *worker) {
  {
    CliLockGuard guard{workers_mutex};

    workers.push_back(worker);
  }

  update();
}

void submit_work(WorkOrder *wo) {
  {
    CliLockGuard guard{orders_mutex};

    bool found = false;
    for (size_t i = 0; i < orders.size(); i++) {
      if (orders[i] == nullptr) {
        wo->slot = i;
        orders[wo->slot] = wo;
        found = true;
      }
    }
    if (!found) {
      wo->slot = orders.size();
      orders.push_back(wo);
    }
  }

  for (auto w : workers) {
    if (w->id == wo->worker) {
      w->accept(wo);
    }
  }

  if (wo->block) {
    task::wait(io_chan);
  }
}

WorkOrder *get_work(work_id_t slot_id) {
  assert(orders[slot_id]);
  return orders[slot_id];
}

void complete_work(work_id_t slot_id) {
  CliLockGuard guard{orders_mutex};

  WorkOrder *wo = orders[slot_id];

  orders[slot_id]->state = WorkOrderState::complete;
  orders[slot_id] = nullptr;

  if (wo->block) {
    task::resume_if(wo->task, io_chan);
  }
}

void Worker::accept(WorkOrder *wo) {
  // This is set first, otherwise the work order could complete before switching to in progress
  wo->state = WorkOrderState::in_progress;
  if (!dev_handle(wo)) {
    wo->state = WorkOrderState::pending;
    pending.push_back(wo);
  }
}

void Worker::update() {
  // Attempt to push work orders into the device
  while (pending.size()) {
    pending[0]->state = WorkOrderState::in_progress;
    if (dev_handle(pending[0])) {
      for (size_t i = 1; i < pending.size(); i++) {
        pending[i - 1] = pending[i];
      }
      pending.pop_back();
    } else {
      pending[0]->state = WorkOrderState::pending;
      break;
    }
  }
}

void update() { task::resume_if(main_thread, main_chan); }

Failable<page::PageRaii> WorkerBackedStorage::read(addr_logical_t addr, uint32_t count) {
  auto page = page::PageRaii(flags, count);

  auto wo = make_unique<WorkOrder>();
  wo->worker = worker;
  wo->block = 1;
  wo->write = 0;
  wo->host = move(page);
  wo->device = addr;
  wo->size = PAGE_SIZE * count;
  storage_worker::submit_work(wo.get());

  assert(wo->state == storage_worker::WorkOrderState::complete);
  page = move(wo->host);

  return Failable<page::PageRaii>(move(page));
}
} // namespace storage_worker
