#ifdef X86_64
.macro pushal
push %rax
push %rbx
push %rcx
push %rdx
push %rsi
push %rdi
push %rbp

push %r8
push %r9
push %r10
push %r11
push %r12
push %r13
push %r14
push %r15
.endm

.macro popal
pop %r15
pop %r14
pop %r13
pop %r12
pop %r11
pop %r10
pop %r9
pop %r8

pop %rbp
pop %rdi
pop %rsi
pop %rdx
pop %rcx
pop %rbx
pop %rax
.endm
#endif

.globl task_asm_enter
task_asm_enter:
#if X86_64
    mov %rdi, %rsp;
#else
    mov %ecx, %esp;
#endif
    popal;
    popf;
    ret;

.globl task_asm_entry_point
task_asm_entry_point:
    sti;
    ret;

.globl task_asm_yield
task_asm_yield:
    pushf;
    pushal;
#if X86_64
    mov %rsp, %rax;
    mov %rdi, %rsp;
    mov %rax, %rdi;
#else
    mov %esp, %eax;
    mov %ecx, %esp;
    push %eax;
#endif
    call task_yield_done;

.globl task_asm_set_stack
task_asm_set_stack:
#if X86_64
    mov %rdi, %rsp;
    push %rsi;
#else
    mov %ecx, %esp;
    push %edx;
#endif
    ret;
