#include <stdbool.h>
#include <stdint.h>

#include "logging/printk.hpp"
#include "main/asm_utils.hpp"
#include "main/cpu.hpp"
#include "main/panic.hpp"
#include "mem/kmem.hpp"
#include "mem/object.hpp"
#include "structures/list.hpp"
#include "structures/mutex.hpp"
#include "structures/shared_ptr.hpp"
#include "task/task.hpp"

extern "C" {
#include "task/asm.h"
}

namespace task {
const uint8_t _INIT_FLAGS = 0x0;
const size_t STACK_PAGES = 0xf;

shared_ptr<Process> kernel_process;

static uint32_t process_counter;
static uint32_t task_counter;

static list<shared_ptr<Process>> processes;

static mutex::Mutex pending_mutex;
static mutex::Mutex _mutex;
static mutex::Mutex wchan_mutex;

list<shared_ptr<Thread>> pending_threads;

vector<Utf8> wchans;
wchan_t no_wchan;

bool inited = false;


void init() {
  kernel_process = make_shared<Process>(0, 0);
  processes.push_back(kernel_process);
  kernel_process->process_id = 0;
  // All other fields 0 by default

  no_wchan = new_wchan(Utf8("."));
  inited = true;
}

shared_ptr<Process> get_process(uint32_t id) {
  for (const auto &p : processes) {
    if (p->process_id == id)
      return p;
  }

  return shared_ptr<Process>();
}


Process::Process(uint32_t owner, uint32_t group)
    : process_id(process_counter++), owner(owner), group(group), thread_counter(0) {}

shared_ptr<Thread> Process::new_thread(addr_logical_t entry_point) {
  shared_ptr<Process> me = get_process(process_id);
  shared_ptr<Thread> t = (threads.emplace_back(make_shared<Thread>(me, entry_point)), threads.back());
  t->wchan = no_wchan;

  pending_mutex.lock();
  t->state_mutex.lock();
  pending_threads.push_front(t);
  t->state = ThreadState::pending;
  t->state_mutex.unlock();
  pending_mutex.unlock();
  return t;
}

shared_ptr<Thread> Process::get_thread(uint32_t id) const {
  for (auto t = threads.cbegin(); t != threads.cend(); t++) {
    if ((*t)->thread_id == id) {
      return *t;
    }
  }
  return shared_ptr<Thread>();
}

void Process::remove_thread(uint32_t id) {
  for (auto t = threads.begin(); t != threads.end(); t++) {
    if ((*t)->thread_id == id) {
      threads.erase(t);
      return;
    }
  }
}


/**
 * @todo Copy all the objects into the new memory map
 * @todo Get the stack object properly
 */
Thread::Thread(shared_ptr<Process> process, addr_logical_t entry)
    : process(process),
      thread_id(++process->thread_counter),
      task_id(++task_counter),
      wchan(no_wchan),
      pending_wchan(no_wchan),
      state(ThreadState::created) {
  bool kernel = process->process_id == 0;
  uinta_t *sp;
#if X86_64
  uint64_t pstate[15] = {};
#else
  uint32_t pstate[8] = {};
#endif
  void *stack_installed;

  // Create the virtual memory map
  vm = make_unique<vm::Map>(process->process_id, task_id, kernel);

  // Create the stack object
  stack = make_shared<object::EmptyObject>(STACK_PAGES, page::PAGE_TABLE_RW, 0, 0);
  bool gen = stack->generate(0, STACK_PAGES);
  assert(gen);

  vm->add_object(stack, TASK_STACK_TOP - PAGE_SIZE * STACK_PAGES, 0, STACK_PAGES);

  object::PageAndOffset lookup = stack->lookup_addr(PAGE_SIZE * STACK_PAGES - 1).val;
  lookup.page.install(0);
  stack_installed = lookup.page.data<void>();

  // Initial stack format:
  // [pushad values]
  // entry eip
  sp = (uinta_t *)((addr_logical_t)stack_installed + (lookup.page_offset + 1) * PAGE_SIZE) - 1;
  *sp = (addr_logical_t)task_end;
  sp--;
  *sp = entry;
  sp--;
  *sp = (addr_logical_t)task_asm_entry_point;
  sp--;
  *sp = _INIT_FLAGS;
  sp -= (sizeof(pstate) / sizeof(pstate[0]));
  memcpy(sp, &pstate, sizeof(pstate));

  stack_pointer = TASK_STACK_TOP - sizeof(void *) * 4 - sizeof(pstate);

  lookup.page.uninstall();
}


Thread::~Thread() {
  // Need to do stuff like free the stack or whatever
  // panic("!");
}

void Thread::end() {
  uint32_t eflags = push_cli();
  _mutex.lock();
  state_mutex.lock();
  state = ThreadState::ended;
  process->remove_thread(thread_id);
  state_mutex.unlock();
  _mutex.unlock();
  pop_flags(eflags);
}


extern "C" void __attribute__((noreturn)) task_enter(shared_ptr<Thread> thread) {
  asm volatile("cli");
  cpu::Status &info = cpu::info();
  uint32_t stack_pointer = thread->stack_pointer;

  thread->vm->enter();
  info.thread = move(thread);

  // And then hop into it
  task_asm_enter(stack_pointer);
}

extern "C" void do_task_yield() {
  push_cli();
  cpu::Status &info = cpu::info();
  assert(info.thread);
  uinta_t stack = (uinta_t)info.stack + PAGE_SIZE;

  // Call the exit function to move over the stack, will call task_yield_done
  task_asm_yield(stack);
}

void task_yield() {
  uint32_t eflags = push_cli();
  cpu::Status &info = cpu::info();
  bool in_thread = (bool)info.thread;

  // Do nothing if we are not in a task
  if (!in_thread) {
    pop_flags(eflags);
    return;
  }

  info.thread->state_mutex.lock();
  info.thread->state = ThreadState::prepending;
  assert(info.thread->wchan == no_wchan);
  info.thread->state_mutex.unlock();

  do_task_yield();
}

extern "C" void __attribute__((noreturn)) task_yield_done(uint32_t sp) {
  asm volatile("cli");
  shared_ptr<Thread> current;
  cpu::Status &info = cpu::info();
  current = info.thread;
  info.thread = nullptr;
  current->stack_pointer = sp;

  // And then use the "normal" memory map
  current->vm->exit();

  current->state_mutex.lock();
  switch (current->state) {
  case ThreadState::prepending:
    // Prepending -> Pending
    pending_mutex.lock();
    pending_threads.push_front(current);
    pending_mutex.unlock();
    current->state = ThreadState::pending;
    break;

  case ThreadState::yielding:
    // Yielding -> Waiting
    current->state = ThreadState::waiting;
    break;

  default:
    panic("[Task] Unknown task state when finished yielding!");
  }
  current->state_mutex.unlock();

  schedule();
}

void __attribute__((noreturn)) schedule() {
  shared_ptr<Thread> next;

  asm volatile("cli");
  cpu::Status &info = cpu::info();
  info.awaiting_schedule = true;
  info.thread = nullptr;

  while (true) {
    if (pending_mutex.trylock()) {
      asm volatile("sti");
      asm volatile("hlt");
      asm volatile("cli");
      continue;
    }

    if (pending_threads.empty()) {
      pending_mutex.unlock();
      asm volatile("sti");
      asm volatile("hlt");
      asm volatile("cli");
      continue;
    }

    break;
  }

  next = pending_threads.back();
  next->state_mutex.lock();
  assert(next->state == ThreadState::pending);
  assert(next->wchan == no_wchan);
  next->state = ThreadState::running;
  pending_threads.pop_back();
  pending_mutex.unlock();

  info.awaiting_schedule = false;
  next->state_mutex.unlock();

  if (true) {
    task_enter(move(next));
  } else {
    schedule();
  }
}


extern "C" void task_timer_yield() {
  cpu::Status &info = cpu::info();

  if (!info.thread && !info.awaiting_schedule) {
    // Not running a thread or waiting for a schedule, do nothing
    return;
  }

  task_yield();
}

extern "C" void __attribute__((noreturn)) task_end_done() {
  shared_ptr<Thread> current = cpu::info().thread;
  cpu::info().thread = nullptr;

  current->end();
  current = nullptr;

  schedule();
}

extern "C" void __attribute__((noreturn)) task_end() {
  asm volatile("cli");

  cpu::Status &info = cpu::info();
  uinta_t stack = (uinta_t)info.stack + PAGE_SIZE;

  task_asm_set_stack(stack, &task_end_done);
  schedule();
}

bool in_thread() {
  bool to_ret = false;

  uint32_t eflags = push_cli();
  cpu::Status &info = cpu::info();
  to_ret = (bool)info.thread;
  pop_flags(eflags);

  return to_ret;
}

shared_ptr<Thread> get_thread() {
  shared_ptr<Thread> to_ret;

  uint32_t eflags = push_cli();
  cpu::Status &info = cpu::info();
  to_ret = info.thread;
  pop_flags(eflags);

  return to_ret;
}

wchan_t new_wchan(Utf8 name) {
  LockGuard guard{wchan_mutex};
  int size = 0;
  wchans.push_back(move(name));
  size = wchans.size() - 1;
  return size;
}

void resume_if(shared_ptr<Thread> thread, wchan_t wchan) {
  CliLockGuard guard{thread->state_mutex};

  switch (thread->state) {
  case ThreadState::waiting:
    // Waiting -> Pending
    assert(thread->wchan == wchan);

    thread->state = ThreadState::pending;
    thread->wchan = no_wchan;

    pending_mutex.lock();
    pending_threads.push_front(thread);
    pending_mutex.unlock();
    break;

  case ThreadState::yielding:
    // Yielding -> Prepending
    thread->state = ThreadState::prepending;
    thread->wchan = no_wchan;
    break;

  case ThreadState::runcontinue:
    // Runcontinue -> Runcontinue
    assert(thread->pending_wchan == wchan);
    break;

  case ThreadState::pending:
    // Pending -> Pending
    break;

  case ThreadState::prepending:
    // Prepending -> Prepending
    break;

  case ThreadState::running:
    // Running -> RunContinue
    thread->state = ThreadState::runcontinue;
    thread->pending_wchan = wchan;
    break;

  case ThreadState::created:
    panic("[Task] Resume_if encountered a thread in a created state");
    break;

  case ThreadState::ended:
    // Do nothing
    break;
  }
}

void wait(wchan_t wchan) {
  bool yield = true;

  uint32_t eflags = push_cli();
  shared_ptr<Thread> t = get_thread();
  t->state_mutex.lock();

  switch (t->state) {
  case ThreadState::runcontinue:
    assert(t->pending_wchan == wchan);
    t->pending_wchan = no_wchan;
    t->state = ThreadState::running;
    yield = false;
    break;

  case ThreadState::running:
    t->wchan = wchan;
    t->state = ThreadState::yielding;
    break;

  default:
    panic("[Task] Thread is in an unknown state on calling wait!");
  }

  t->state_mutex.unlock();
  pop_flags(eflags);

  if (yield) {
    do_task_yield();
  }
}
} // namespace task
