#include <stdint.h>

#include "int/ioapic.hpp"

#include "int/idt.hpp"
#include "logging/printk.hpp"
#include "mem/page.hpp"
#include "structures/buffer.hpp"

extern "C" {
#include "hw/ports.h"
#include "hw/utils.h"
#include "int/numbers.h"
}

namespace ioapic {
const uint8_t _DATA = 0x4 /* 4 bytes */;
const uint8_t _ADDR = 0x0;

const uint8_t _REG_ID = 0x0;
const uint8_t _REG_VER = 0x1;
const uint8_t _REG_ARB = 0x2;
#define _REG_IRQ(x) ((x)*2 + 0x10)

static Buffer<volatile uint32_t> _base;

static void _write(uint32_t reg, uint32_t value) {
  _base[_ADDR] = reg;
  _base[_DATA] = value;
}

static uint32_t _read(uint32_t reg) {
  _base[_ADDR] = reg;
  return _base[_DATA];
}

void init() {
  page::Page *page;

  page = page::create(0xfec00000, page::FLAG_KERNEL, 1);
  _base = Buffer<volatile uint32_t>(page::kinstall(page, page::PAGE_TABLE_CACHEDISABLE | page::PAGE_TABLE_RW),
                                    PAGE_SIZE / sizeof(uint32_t));

  printk("IOAPIC ID: %x, Version: %x (%x)\n", _read(_REG_ID), _read(_REG_VER), _read(_REG_IRQ(0)));
}

void enable(uint8_t irq, uint8_t vector, uint64_t flags) { _write(_REG_IRQ(irq), vector | flags); }

void disable(uint8_t irq) { _write(_REG_IRQ(irq), MASK); }

void enable_func(uint8_t irq, idt::interrupt_handler_t func, uint64_t flags) {
  idt::install(INT_IOAPIC_BASE + irq, func, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  enable(irq, INT_IOAPIC_BASE + irq, flags);
}

void keyboard(idt_proc_state_t state, addr_logical_t ret) {
  (void)state;
  printk("Got keyboard input!\n");
}

#undef _REG_IRQ
} // namespace ioapic
