#include <stdint.h>

#include "int/exceptions.hpp"
#include "int/idt.hpp"
#include "main/common.hpp"
#include "main/cpu.hpp"
#include "main/panic.hpp"

extern "C" {
#include "int/numbers.h"
}

namespace exceptions {
void div0(idt_proc_state_t state, addr_logical_t ret) { panic_at(state.reg_bp, ret, "Division by 0"); }

void debug(idt_proc_state_t state, addr_logical_t ret) { panic_at(state.reg_bp, ret, "Debug Exception"); }

void nmi(idt_proc_state_t state, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "NMI Received (yer computer just don' broke");
}

void overflow(idt_proc_state_t state, addr_logical_t ret) { panic_at(state.reg_bp, ret, "Overflow Exception"); }

void bound_range_exceeded(idt_proc_state_t state, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "Bound Range Exceeded Exception");
}

void invalid_opcode(idt_proc_state_t state, addr_logical_t ret) { panic_at(state.reg_bp, ret, "Invalid Opcode"); }

void double_fault(idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "Double Fault %x", errcode);
}

void invalid_tss(idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "Invalid TSS %x", errcode);
}

void segment_not_present(idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "Segment not Present %x", errcode);
}

void stack_segment_not_present(idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  (void)errcode;
  panic_at(state.reg_bp, ret, "Stack-Segment not Present %x", errcode);
}

void gpf(idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "General Protection Fault %x", errcode);
}

void page_fault(idt_proc_state_t state, uint32_t errcode, addr_logical_t ret) {
  uinta_t addr;
  __asm__("mov %%cr2, %0" : "=r"(addr));

  if (!(cpu::inited && cpu::current_thread() && cpu::current_thread()->vm->resolve_fault(addr))) {
    panic_at(state.reg_bp, ret, "Unresolved Page Fault %x [Address: %p]", errcode, addr);
  }
}

void floating_point(idt_proc_state_t state, addr_logical_t ret) {
  panic_at(state.reg_bp, ret, "Floating Point Exception");
}

void init() {
  idt::install(INT_DIV0, div0, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_DEBUG, debug, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_NMI, nmi, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_OVERFLOW, overflow, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_BOUND_RANGE_EXCEEDED, bound_range_exceeded, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_INVALID_OPCODE, invalid_opcode, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install_with_error(INT_DOUBLE_FAULT, double_fault, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install_with_error(INT_INVALID_TSS, invalid_tss, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install_with_error(INT_SEGMENT_NOT_PRESENT, segment_not_present, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install_with_error(INT_STACK_SEGMENT_NOT_PRESENT, stack_segment_not_present, GDT_SELECTOR(0, 0, 2),
                          idt::GATE_32_INT);
  idt::install_with_error(INT_GPF, gpf, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install_with_error(INT_PAGE_FAULT, page_fault, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
  idt::install(INT_FLOATING_POINT, floating_point, GDT_SELECTOR(0, 0, 2), idt::GATE_32_INT);
}
} // namespace exceptions
