.globl push_flags
push_flags:
    pushf;
#if X86_64
    pop %rax;
#else
    pop %eax;
#endif
    ret;

.globl push_cli
push_cli:
    pushf;
    cli;
#if X86_64
    pop %rax;
#else
    pop %eax;
#endif
    ret;

.globl pop_flags
pop_flags:
#if X86_64
    push %rdi;
#else
    push %ecx;
#endif
    popf;
    ret;
