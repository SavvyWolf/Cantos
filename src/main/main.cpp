#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "debug/kernel_elf.hpp"
#include "display/display.hpp"
#include "display/logdisplay.hpp"
#include "fs/expanse_fs.hpp"
#include "fs/physical_mem_storage.hpp"
#include "fs/storage_worker.hpp"
#include "hw/cmos.hpp"
#include "hw/pci/pci.hpp"
#include "hw/pit.hpp"
#include "hw/ps2.hpp"
#include "int/exceptions.hpp"
#include "int/idt.hpp"
#include "int/ioapic.hpp"
#include "int/lapic.hpp"
#include "int/pic.hpp"
#include "logging/printk.hpp"
#include "main/cmdline.hpp"
#include "main/cpu.hpp"
#include "main/loader_data.hpp"
#include "main/multiboot.hpp"
#include "main/panic.hpp"
#include "main/vga.hpp"
#include "mem/gdt.hpp"
#include "mem/kmem.hpp"
#include "mem/object.hpp"
#include "mem/page.hpp"
#include "structures/elf.hpp"
#include "structures/mutex.hpp"
#include "task/task.hpp"
#include "test/test.hpp"

extern "C" {
#include "hw/acpi.hpp"
#include "hw/serial.h"
#include "int/numbers.h"
}

#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif

#if !defined(__i386__) && !X86_64
#error "Wrong architecture!"
#endif

#if !defined(__x86_64__) && X86_64
#error "Wrong architecture!"
#endif

extern char _endofelf;
extern "C" void _init();

void main_thread() {
  CHECK_IF_SET;
  cmos::init();
  lapic::awaken_others();
  storage_worker::init();
  ps2::init();
  pci::init();
  pci::print_devices();

  list<test::TestResult> res = test::run_tests();
  test::print_results(res, true);

  display::Display &d = vga::addDisplay<log_display::LogDisplay>();
  vga::switchDisplay(d.id);

  auto s = make_shared<storage_worker::WorkerBackedStorage>(0, 0, page::FLAG_KERNEL);
  shared_ptr<expanse_fs::ExpanseFs> fs = make_shared<expanse_fs::ExpanseFs>(s);

  shared_ptr<filesystem::FilePathEntry> root =
      make_shared<filesystem::FilePathEntry>(Utf8(""), nullptr, fs->root_inode().val);

  shared_ptr<filesystem::FilePathEntry> child = parse_path(Utf8("contents"), root);
  filesystem::FilePathEntry error_loc;
  child->populate(error_loc);

  vm::BaseMap *map = cpu::current_thread()->vm.get();
  map->add_object(child->get_inode()->contents, 0x8000, 0x0, 0x100);

  printk("Option test3: %s\n", cmdline::get_option_string("test3", "notset").to_string());
}

extern "C" void __attribute__((noreturn)) kernel_main() {
#if !X86_64
  loader_data::passed_loader_data = {};
#endif
  kmem::init(loader_data::passed_loader_data);

  _init();

  cmdline::parse(multiboot::multiboot_data.cmdline);

  serial_init();
  idt::init();
  idt::setup();
  exceptions::init();

  vga::init();
  printk("SavOS\n");
  printk("Booted by %s [%s]\n", multiboot::multiboot_data.boot_loader_name, multiboot::multiboot_data.cmdline);
#if DEBUG_MAP
  printk("Initial memory state:\n");
  printk("Kernel start: %x\n", kmem::map.kernel_ro_start);
  printk("Kernel end: %x\n", kmem::map.kernel_rw_end);
  printk("Kernel symbol info start: %x\n", kmem::map.kernel_info_start);
  printk("Kernel symbol info end: %x\n", kmem::map.kernel_info_end);
  printk("Kernel VM table start: %x\n", kmem::map.vm_start);
  printk("Kernel VM table end: %x\n", kmem::map.vm_end);
  printk("Memory start: %x\n", kmem::map.memory_start);
  printk("Memory end: %x\n", kmem::map.memory_end);
#endif

  acpi::init();

  if (acpi::acpi_found) {
    printk("Machine has %d cores and %d ioapics\n", acpi::proc_count, acpi::ioapic_count);
  } else {
    kwarn("Could not find acpi information\n");
  }

  cpu::init();
  pic::init();
  lapic::init();
  lapic::setup();
  ioapic::init();
  pit::init();

  task::init();

#if X86_64
#else
  if (multiboot::multiboot_data.header.flags & (1 << 5)) {
    kernel_elf::load_kernel_elf(multiboot::multiboot_data.header.elf_num, multiboot::multiboot_data.header.elf_size,
                                multiboot::multiboot_data.header.elf_addr, multiboot::multiboot_data.header.elf_shndx);
  }
#endif

  /*while(1) {
      page::Page *page = page::alloc(0, 1);
      void *loc = page::kinstall(page, 0);
      //loc = page::kinstall(page, 0);
      page_kuninstall(loc, page);
      page_free(page);
  }*/

  task::kernel_process->new_thread((addr_logical_t)&main_thread);
  task::schedule();
}

#if X86_64
// The page table for application processors to use
extern "C" {
extern volatile uint32_t low_ap_64_page_table;
}

extern "C" void kernel_main_64(loader_data::LoaderData *loader_data_obj) {
  gdt::gdt_init();
  gdt::gdt_setup();

  loader_data::passed_loader_data = *loader_data_obj;
  low_ap_64_page_table = loader_data::passed_loader_data.pml4_page;
  multiboot::multiboot_data = loader_data_obj->multiboot_data;
  kernel_main();
}
#endif

extern "C" void __attribute__((noreturn)) ap_main() {
  cpu::info().awoken = true;

  idt::setup();
  lapic::setup();

  asm("sti");

  task::schedule();
}
