#include "structures/shared_ptr.hpp"
#include "test/test.hpp"

namespace _tests {
class SharedPtrTest : public test::TestCase {
public:
  SharedPtrTest() : test::TestCase("shared_ptr Test"){};

  void run_test() override {
    test("Constructing");
    shared_ptr<int> ap = make_shared<int>(7);

    tassert(ap);
    tassert(*ap == 7);

    test("Moving a pointer");
    shared_ptr<int> bp = shared_ptr<int>(move(ap));
    tassert(bp);
    tassert(!ap);
    tassert(*bp == 7);

    ap = move(bp);
    tassert(ap);
    tassert(!bp);
    tassert(*ap == 7);

    test("Resetting a pointer");
    int *b = new int(8);
    ap.reset(b);
    tassert(*ap == 8);

    int *a = new int(7);
    bp.reset(a);
    tassert(*bp == 7);

    test("Swapping pointers");
    ap.swap(bp);
    tassert(*bp == 8);
    tassert(*ap == 7);

    bp = nullptr;
    ap.swap(bp);
    tassert(!ap);
    tassert(*bp == 7);

    ap.swap(bp);
    tassert(!bp);
    tassert(*ap == 7);

    test("make_unique");
    bp = make_shared<int>(9);
    tassert(*bp == 9);

    test("Shared assignment");
    uint8_t constructs = 0;
    uint8_t destructs = 0;
    class TestClass {
    public:
      int val;
      uint8_t &destructs;
      TestClass(int i, uint8_t &constructs, uint8_t &destructs) : val(i), destructs(destructs) { constructs++; }
      ~TestClass() { destructs++; }
    };

    shared_ptr<TestClass> cp = make_shared<TestClass>(2, constructs, destructs);
    tassert(cp);
    tassert(constructs == 1);
    tassert(destructs == 0);
    tassert(cp->val == 2);

    shared_ptr<TestClass> dp = shared_ptr<TestClass>(cp);
    tassert(dp);
    tassert(cp);
    tassert(constructs == 1);
    tassert(destructs == 0);
    tassert(dp->val == 2);

    cp = nullptr;
    tassert(dp);
    tassert(!cp);
    tassert(constructs == 1);
    tassert(destructs == 0);
    tassert(dp->val == 2);

    cp = dp;
    tassert(dp);
    tassert(constructs == 1);
    tassert(destructs == 0);
    tassert(cp->val == 2);

    cp = nullptr;
    dp = nullptr;
    tassert(constructs == 1);
    tassert(destructs == 1);
  }
};

test::AddTestCase<SharedPtrTest> sharedPtrTest;
} // namespace _tests
