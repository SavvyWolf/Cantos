#include <stdint.h>

#include "logging/printk.hpp"
#include "main/common.hpp"
#include "mem/kmem.hpp"
#include "structures/elf.hpp"

namespace elf {
#define _WIDTH(X) (is64Bit() ? width64.X : width32.X)
bool Header::is64Bit() const { return ident[EI_CLASS] == 2; }

SectionHeader *Header::sectionHeader(uint32_t id) const {
  return (SectionHeader *)((addr_logical_t)this + _WIDTH(shoff) + (id * _WIDTH(shentsize)));
}

SectionHeader *Header::sectionStringSection() const { return sectionHeader(_WIDTH(shstrndx)); }

void *Header::sectionData(uint32_t id) const {
  SectionHeader *hdr = sectionHeader(id);

  if (is64Bit()) {
    return (void *)((addr_logical_t)this + hdr->width64.offset);
  } else {
    return (void *)((addr_logical_t)this + hdr->width32.offset);
  }
}

void *Header::runtimeSectionData(uint32_t id) const {
  SectionHeader *hdr = sectionHeader(id);

  if (is64Bit()) {
    return (void *)hdr->width64.addr;
  } else {
    return (void *)hdr->width32.addr;
  }
}

void *Header::sectionStringSectionData() const { return sectionData(_WIDTH(shstrndx)); }

void *Header::runtimeSectionStringSectionData() const { return runtimeSectionData(_WIDTH(shstrndx)); }

char *Header::lookupString(uint32_t section, uint32_t offset) const { return &((char *)sectionData(section))[offset]; }

char *Header::runtimeLookupString(uint32_t section, uint32_t offset) const {
  return (char *)runtimeSectionData(section) + offset;
}

char *Header::lookupSectionString(uint32_t offset) const { return lookupString(_WIDTH(shstrndx), offset); }

char *Header::runtimeLookupSectionString(uint32_t offset) const {
  return runtimeLookupString(_WIDTH(shstrndx), offset);
}

uint32_t Header::sectionByType(SectionType type, uint32_t base) const {
  for (; sectionHeader(base)->type != type && base < _WIDTH(shnum); base++) {
    // Pass
  }

  if (sectionHeader(base)->type == type) {
    return base;
  }

  return 0;
}

Symbol *Header::symbol(uint32_t section, uint32_t offset) const {
  SectionHeader *sect = sectionHeader(section);
  Symbol *symbols = (Symbol *)sectionData(section);

  if (offset >= entriesInSection(section)) {
    return 0;
  }

  if (is64Bit()) {
    return (Symbol *)((addr_logical_t)symbols + offset * sect->width64.entsize);
  } else {
    return (Symbol *)((addr_logical_t)symbols + offset * sect->width32.entsize);
  }
}

Symbol *Header::runtimeSymbol(uint32_t section, uint32_t offset) const {
  SectionHeader *sect = sectionHeader(section);
  Symbol *symbols = (Symbol *)runtimeSectionData(section);

  if (offset >= entriesInSection(section)) {
    return 0;
  }

  if (is64Bit()) {
    return (Symbol *)((addr_logical_t)symbols + offset * sect->width64.entsize);
  } else {
    return (Symbol *)((addr_logical_t)symbols + offset * sect->width32.entsize);
  }
}

char *Header::symbolName(uint32_t section, uint32_t offset) const {
  SectionHeader *sect = sectionHeader(section);
  Symbol *symbol = this->symbol(section, offset);

  if (is64Bit()) {
    return lookupString(sect->width64.link, symbol->name);
  } else {
    return lookupString(sect->width32.link, symbol->name);
  }
}

char *Header::runtimeSymbolName(uint32_t section, uint32_t offset) const {
  SectionHeader *sect = sectionHeader(section);
  Symbol *symbol = runtimeSymbol(section, offset);

  if (is64Bit()) {
    return lookupString(sect->width64.link, symbol->name);
  } else {
    return lookupString(sect->width32.link, symbol->name);
  }
}

uint32_t Header::runtimeFindSymbolId(uint32_t addr, uint32_t type) const {
  uint32_t section_id = sectionByType(SectionType::SYMTAB, 0);
  uint32_t best_id = 0;
  uint32_t delta = 0xffffffff;

  for (uint32_t i = 0; i < entriesInSection(section_id); i++) {
    Symbol *curr = runtimeSymbol(section_id, i);

    if (curr->info != type) {
      continue;
    }

    if (curr->value <= addr && ((addr - curr->value) < delta)) {
      best_id = i;
      delta = addr - curr->value;
    }
  }

  return best_id;
}

Symbol *Header::runtimeFindSymbol(uint32_t addr, uint32_t type) const {
  uint32_t section_id = sectionByType(SectionType::SYMTAB, 0);

  return runtimeSymbol(section_id, runtimeFindSymbolId(addr, type));
}

char *Header::runtimeFindSymbolName(uint32_t addr, uint32_t type) const {
  uint32_t section_id = sectionByType(SectionType::SYMTAB, 0);

  return runtimeSymbolName(section_id, runtimeFindSymbolId(addr, type));
}

size_t Header::numSections() const { return _WIDTH(shnum); }

uint32_t Header::entriesInSection(uint32_t id) const {
  if (is64Bit()) {
    return sectionHeader(id)->width64.size / sectionHeader(id)->width64.entsize;
  } else {
    return sectionHeader(id)->width32.size / sectionHeader(id)->width32.entsize;
  }
}
#undef _WIDTH

uint8_t st_bind(uint8_t x) { return x << 4; }
uint8_t st_type(uint8_t x) { return x & 0xf; }
uint8_t st_info(uint8_t b, uint8_t t) { return st_bind(b) + st_type(t); }
} // namespace elf
