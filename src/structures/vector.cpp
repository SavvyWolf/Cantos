#include "main/common.hpp"
#include "test/test.hpp"

namespace _tests {
class VectorTest : public test::TestCase {
public:
  VectorTest() : test::TestCase("Vector Test"){};

  virtual void run_test() override {
    test("Constructors");
    vector<int> a = vector<int>();
    tassert(a.size() == 0);
    tassert(a.empty());

    vector<int> b = vector<int>(5, 0);
    tassert(b.size() == 5);
    tassert(b.front() == 0);
    tassert(!b.empty());

    test("Emptying a vector");
    b.clear();
    tassert(b.size() == 0);
    tassert(b.empty());

    test("Back pushing a vector");
    b.push_back(1);
    tassert(b.size() == 1);
    tassert(!b.empty());
    tassert(b.front() == 1);
    tassert(b.back() == 1);

    b.push_back(2);
    tassert(b.size() == 2);
    tassert(b.front() == 1);
    tassert(b.back() == 2);

    test("Back popping a vector");
    b.push_back(2);
    b.push_back(3);
    b.pop_back();
    tassert(b.size() == 3);
    tassert(b.front() == 1);
    tassert(b.back() == 2);

    b.pop_back();
    b.pop_back();
    b.pop_back();
    tassert(b.empty());
    tassert(b.size() == 0);

    test("Iterating through a vector");
    b.push_back(1);
    b.push_back(2);
    b.push_back(3);
    b.push_back(4);
    b.push_back(5);

    uint8_t seeking = 0;
    for (int e : b) {
      tassert(e == (++seeking));
    }

    test("Emplace");
    uint8_t constructs = 0;
    class TestClass {
    public:
      int val;
      TestClass(int i, uint8_t &const_count) : val(i) { const_count++; }
    };

    vector<TestClass> c;
    c.emplace_back(1, constructs);
    tassert(c[0].val == 1);

    c.clear();
    tassert(c.empty());
    tassert(constructs == 1);
  }
};

test::AddTestCase<VectorTest> vectorTest;
} // namespace _tests
