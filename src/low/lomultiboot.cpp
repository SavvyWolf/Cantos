#include "low/lomultiboot.hpp"
#include "main/common.hpp"

using namespace multiboot;

static void *_memcpy(void *destination, const void *source, size_t num) {
  size_t i;
  for (i = 0; i < num; i++) {
    ((char *)destination)[i] = ((char *)source)[i];
  }
  return destination;
}

static char *_strncpy(char *destination, const char *source, size_t n) {
  size_t i;
  for (i = 0; i < n; i++) {
    destination[i] = source[i];
    if (destination[i] == '\0') {
      break;
    }
  }
  return destination;
}

extern "C" void populate_multiboot(MultibootData *data, info_t *mbi) {
  _strncpy(reinterpret_cast<char *>(&data->cmdline), reinterpret_cast<char *>(mbi->cmdline), LOCAL_CMDLINE_LENGTH);
  data->cmdline[LOCAL_CMDLINE_LENGTH - 1] = '\0';

  _strncpy(reinterpret_cast<char *>(&data->boot_loader_name), reinterpret_cast<char *>(mbi->boot_loader_name),
           LOCAL_BOOT_LOADER_NAME_LENGTH);
  data->boot_loader_name[LOCAL_BOOT_LOADER_NAME_LENGTH - 1] = '\0';

  auto mm_entry = mbi->mmap_addr;
  for (size_t i = 0; mm_entry - mbi->mmap_addr < mbi->mmap_length && i < LOCAL_MM_COUNT; i++) {
    _memcpy(&data->mem_table[i], reinterpret_cast<entry_t *>(mm_entry), sizeof(multiboot::entry_t));
    mm_entry = mm_entry + reinterpret_cast<entry_t *>(mm_entry)->size + 4;
  }

  _memcpy(&data->header, mbi, sizeof(multiboot::info_t));
}
