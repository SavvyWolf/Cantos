#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "hw/acpi.hpp"
#include "low/loerror.hpp"
#include "low/lomultiboot.hpp"
#include "main/loader_data.hpp"
#include "mem/kmem.hpp"
#include "mem/page.hpp"
#include "structures/elf.hpp"

__attribute__((aligned(0x1000))) volatile page::width64::page_table_entry_t
    _page_table_entries[PAGE_TABLE_LENGTH_64 * PAGE_TABLE_LENGTH_64];

extern "C" char _endofkernel64;
extern "C" elf::Header _64_kernel;
extern "C" char _lo_stack_bottom;
extern "C" {
uint32_t entry_point_hi;
uint32_t entry_point_lo;

loader_data::LoaderData loader_data_obj;
}

namespace {
uint64_t cursor;

uint64_t produce_page() {
  cursor += PAGE_SIZE;
  cursor = loader_data_obj.multiboot_data.advance_to_next_usable(cursor);
  return cursor;
}

uint64_t produce_clean_page() {
  uint64_t page = produce_page();
  uint64_t *page_data = reinterpret_cast<uint64_t *>(page);

  for (size_t i = 0; i < PAGE_SIZE / sizeof(uint64_t); i++) {
    page_data[i] = 0;
  }
  return page;
}
} // namespace

extern "C" volatile void *create_big_table(multiboot::info_t *mbi) {
  using namespace page::width64;

  // Check for 64b support
  uint32_t volatile support;

  __asm__ volatile("\
        mov $0x80000001, %%eax\n\
        cpuid\n\
        shr $29, %%edx\n\
        and $1, %%edx\n\
        mov %%edx, %0"
                   : "=r"(support)
                   : /* Nothing */
                   : "eax", "ebx", "ecx", "edx");

  if (!support) {
    low_error("64 bit support not found");
  }

  // Store multiboot data
  populate_multiboot(&loader_data_obj.multiboot_data, mbi);

  // Set the cursor to start allocating pages from
  cursor = (reinterpret_cast<uint64_t>(&_endofkernel64) - 1) & ~(PAGE_SIZE - 1); // One before the last block

  // Get the entry point from the elf
  entry_point_hi = _64_kernel.width64.entry >> 32;
  entry_point_lo = _64_kernel.width64.entry & 0xffffffff;

  // Create the kernel Page dir
  auto dir = reinterpret_cast<page_dir_t *>(produce_clean_page());
  loader_data_obj.dir_high_page = reinterpret_cast<addr_phys_t>(dir);

  // Create a kernel PDP table
  auto pdp = reinterpret_cast<page_dir_pointer_t *>(produce_clean_page());
  pdp->entries[PAGE_TABLE_LENGTH_64 - 1] = reinterpret_cast<int64_t>(dir) | page::PAGE_TABLE_PRESENT;
  loader_data_obj.pdp_high_page = reinterpret_cast<addr_phys_t>(pdp);

  // And a low PDP table
  auto low_pdp = reinterpret_cast<page_dir_pointer_t *>(produce_clean_page());
  low_pdp->entries[0] = 0 | page::PAGE_TABLE_SIZE | page::PAGE_TABLE_RW | page::PAGE_TABLE_PRESENT;
  loader_data_obj.pdp_low_page = reinterpret_cast<addr_phys_t>(low_pdp);

  // Create a PML4 table
  auto pml4 = reinterpret_cast<page_pml4_t *>(produce_clean_page());
  pml4->entries[PAGE_TABLE_LENGTH_64 - 1] = reinterpret_cast<int64_t>(pdp) | page::PAGE_TABLE_PRESENT;
  pml4->entries[0] = reinterpret_cast<int64_t>(low_pdp) | page::PAGE_TABLE_PRESENT;
  loader_data_obj.pml4_page = reinterpret_cast<addr_phys_t>(pml4);

  // Prepopluate the table with the tables in _page_table_entries
  for (size_t i = 0; i < PAGE_TABLE_LENGTH_64; i++) {
    dir->entries[i] =
        reinterpret_cast<int64_t>(&_page_table_entries[i * PAGE_TABLE_LENGTH_64]) | page::PAGE_TABLE_PRESENT;
  }

  uint64_t highest_entry = 0;

  // Load the elf information from each section
  for (size_t i = 0; i < _64_kernel.numSections(); i++) {
    elf::SectionHeader *hdr = _64_kernel.sectionHeader(i);

    // Get section information
    bool alloc = false;
    bool writeable = hdr->width64.flags & elf::SHF_WRITE;

    if (hdr->type == elf::SectionType::NOBITS) {
      alloc = true;
    } else {
      if (hdr->type != elf::SectionType::PROGBITS)
        continue;
    }
    if (!(hdr->width64.flags & elf::SHF_ALLOC))
      continue;

    // Calculate addresses and sizes
    int64_t dest = hdr->width64.addr;
    int64_t source = reinterpret_cast<int64_t>(_64_kernel.sectionData(i));
    int64_t size = hdr->width64.size;
    if (!dest)
      low_error("Unknown load address!?");

    // Sections under 1MB get copied into the appropriate place, rather than mapped
    if (dest < 0x100000 && dest > 0x0) {
      for (size_t i = 0; i < size; i++) {
        *reinterpret_cast<char *>(dest + i) = *reinterpret_cast<char *>(source + i);
      }
      continue;
    }

    // Populate memory tables with data in that section
    while (size > 0) {
      uint64_t dest_entry = (dest >> 12) & ((1 << 18) - 1);

      if (alloc) {
        source = produce_clean_page();
      }
      _page_table_entries[dest_entry] =
          (source & ~0xfff) | page::PAGE_TABLE_PRESENT | (writeable ? page::PAGE_TABLE_RW : 0);

      if (highest_entry < dest_entry)
        highest_entry = dest_entry;

      size -= PAGE_SIZE;
      source += PAGE_SIZE;
      dest += PAGE_SIZE;
    }
  }

  // Load the page tables into high memory
  highest_entry++;
  loader_data_obj.upper_page_dir = KERNEL_VM_BASE_64 + (highest_entry * PAGE_SIZE);
  for (size_t i = 0; i < PAGE_TABLE_LENGTH_64; i++) {
    _page_table_entries[highest_entry] = reinterpret_cast<int64_t>(&_page_table_entries[i * PAGE_TABLE_LENGTH_64]) |
                                         page::PAGE_TABLE_RW | page::PAGE_TABLE_CACHEDISABLE | page::PAGE_TABLE_PRESENT;
    highest_entry++;
  }

  loader_data_obj.allocation_start = cursor + PAGE_SIZE;
  loader_data_obj.stack_page = reinterpret_cast<uint64_t>(&_lo_stack_bottom);

  return pml4;
}
