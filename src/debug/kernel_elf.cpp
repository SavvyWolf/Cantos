#include <stdint.h>

#include "debug/kernel_elf.hpp"
#include "structures/elf.hpp"

namespace kernel_elf {
using namespace elf;

void load_kernel_elf(uint32_t num, uint32_t size, addr_logical_t addr, uint32_t shndx) {
  kernel_elf = (Header *)kmem::kmalloc((num * size) + sizeof(Header), 0);

  kernel_elf->width32.shnum = num;
  kernel_elf->width32.shentsize = size;
  kernel_elf->width32.shoff = sizeof(Header);
  kernel_elf->width32.shstrndx = shndx;

  memcpy((void *)((addr_logical_t)kernel_elf + sizeof(Header)), (void *)addr, (num * size));

  // Now go through all the headers and update their addresses if needed
  for (uint32_t i = 0; i < num; i++) {
    SectionHeader *hdr = kernel_elf->sectionHeader(i);

    if (hdr->width32.addr && hdr->width32.addr < KERNEL_VM_BASE) {
      hdr->width32.addr += KERNEL_VM_BASE;
    }
  }
}

Header *kernel_elf;
} // namespace kernel_elf
