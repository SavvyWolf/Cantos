#include "utils/text.hpp"

namespace utils_text {
vector<size_t> ascii_break_into_lines(Utf8 text, size_t width) {
  vector<size_t> breaks = {};

  size_t word_start = 0;
  size_t p = 0;
  size_t line_p = 0;

  auto is_ws = [&](char c) { return c == ' ' || c == '\t' || c == '\n' || c == '\0'; };
  auto skip_to_next_ws = [&]() {
    while (!is_ws(text[p])) {
      p++;
      line_p++;
    }
  };

  while (p < text.bytes()) {
    word_start = p;
    skip_to_next_ws();

    if (text[p] == '\n' || line_p >= width) {
      // Need to break text on word start
      breaks.emplace_back(word_start);
      line_p = 0;
    } else {
      // All okay!
      line_p++;
      p++;
    }
  }

  return breaks;
}
} // namespace utils_text
