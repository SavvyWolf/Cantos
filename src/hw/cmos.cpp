#include "hw/cmos.hpp"
#include "hw/ports.h"
#include "hw/utils.h"

namespace cmos {
const uint8_t REG_SECOND = 0x00;
const uint8_t REG_MINUTE = 0x02;
const uint8_t REG_HOUR = 0x04;
const uint8_t REG_DOM = 0x07;
const uint8_t REG_MONTH = 0x08;
const uint8_t REG_YEAR = 0x09;
const uint8_t REG_STATUS_A = 0x0a;
const uint8_t REG_STATUS_B = 0x0b;

bool waiting_to_reset;
uint8_t reset_timer;
volatile Time utc;
unsigned int counter;
bool twentyfour_hour;
bool binary;

void init() {
  utc.subsecond = 0;
  utc.second = 0;
  utc.minute = 0;
  utc.hour = 0;
  utc.dom = 1;
  utc.month = 1;
  utc.year = 0;
  utc.century = 20;

  uint8_t config = read_data(REG_STATUS_B);
  twentyfour_hour = config & 0x02;
  binary = config & 0x04;
  sched_read_time();
}

// From http://howardhinnant.github.io/date_algorithms.html#days_from_civil
int32_t _days_from_civil(int16_t y, uint8_t m, uint8_t d) {
  y -= m <= 2;
  const uint16_t era = (y >= 0 ? y : y - 399) / 400;
  const uint16_t yoe = static_cast<uint16_t>(y - era * 400);           // [0, 399]
  const uint16_t doy = (153 * (m + (m > 2 ? -3 : 9)) + 2) / 5 + d - 1; // [0, 365]
  const uint32_t doe = yoe * 365 + yoe / 4 - yoe / 100 + doy;          // [0, 146096]
  return era * 146097 + doe - 719468;
}

int64_t cmos::Time::epoch_time() const volatile {
  int32_t cdays = _days_from_civil(century * 100 + year, month, dom);
  int32_t cseconds = (((hour * 60) + minute) * 60) + second;
  return (cdays * 24 * 60 * 60) + cseconds;
}

int64_t cmos::Time::epoch_time_us() const volatile {
  return epoch_time() * 1000000 + ((subsecond * 1000000) / pit::PER_SECOND);
}

uint8_t _decode_value(uint8_t val) {
  if (binary)
    return val;
  return ((val / 16) * 10) + (val & 0xf);
}

bool _is_leap_year(uint8_t year, uint8_t century) {
  if (year % 4)
    return false;
  if (year != 0)
    return true;
  if (century % 4)
    return false;
  return true;
}

Time _read_time() {
  Time ret = {};
  ret.second = _decode_value(read_data(REG_SECOND));
  ret.minute = _decode_value(read_data(REG_MINUTE));
  uint8_t hour = _decode_value(read_data(REG_HOUR));
  if (twentyfour_hour) {
    ret.hour = _decode_value(hour);
  } else {
    bool pm = hour &= 0x80;
    hour = _decode_value(hour & 0x7f);
    if (hour == 12)
      hour = 0;
    if (pm)
      hour += 12;
    ret.hour = hour;
  }
  ret.hour = _decode_value(read_data(REG_HOUR));
  ret.dom = _decode_value(read_data(REG_DOM));
  ret.month = _decode_value(read_data(REG_MONTH));
  ret.year = _decode_value(read_data(REG_YEAR));
  ret.century = 20;
  return ret;
}

bool _attempt_to_update_clock() {
  Time t1;
  Time t2;
  if (read_data(REG_STATUS_A) & 0x80)
    return false;
  t1 = _read_time();
  if (read_data(REG_STATUS_A) & 0x80)
    return false;
  t2 = _read_time();
  if (t1 == t2)
    utc = t1;
  return true;
}

void increment() {
  if (waiting_to_reset) {
    if (_attempt_to_update_clock()) {
      waiting_to_reset = false;
      return;
    }
  }

  utc.subsecond++;

  counter++;
  if (counter >= pit::PER_SECOND) {
    counter = 0;
    utc.subsecond = 0;
    utc.second++;
    if (utc.second != 60)
      return;
    utc.second = 0;
    utc.minute++;
    reset_timer--;
    if (reset_timer == 0)
      sched_read_time();
    if (utc.minute != 60)
      return;
    utc.minute = 0;
    utc.hour++;
    if (utc.hour != 24)
      return;
    utc.hour = 0;
    utc.dom++;
    uint8_t days = DAYS_IN_MONTH[utc.month];
    if (!days) {
      assert(utc.month == 2);
      days = _is_leap_year(utc.year, utc.century) ? 29 : 28;
    }
    if (utc.dom != days + 1)
      return;
    utc.dom = 1;
    utc.month++;
    if (utc.month != 13)
      return;
    utc.month = 1;
    utc.year++;
    if (utc.year != 100)
      return;
    utc.year = 0;
    utc.century++;
  }
}

void sched_read_time() {
  waiting_to_reset = true;
  reset_timer = UPDATE_MINUTES;
}

uint8_t read_data(uint8_t reg) {
  // TODO: This and in writing data assumes NMI must always be set, check this!
  outb(IO_PORT_CMOS_ADDRESS, reg);
  io_wait();
  return inb(IO_PORT_CMOS_DATA);
}

void write_data(uint8_t reg, uint8_t data) {
  outb(IO_PORT_CMOS_ADDRESS, reg);
  io_wait();
  outb(IO_PORT_CMOS_DATA, data);
}
} // namespace cmos
