#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "fs/storage_worker.hpp"
#include "hw/pci/pci.hpp"
#include "hw/pit.hpp"
#include "main/common.hpp"
#include "structures/buffer.hpp"

using namespace pci;

namespace {

const uint32_t BYTES_PER_PRDT = 1024 * 1024 * 4;
const uint32_t SECTOR_SIZE = 512; // TODO

// ATA commands
const uint8_t ATA_CMD_READ_DMA_EX = 0x25;
const uint8_t ATA_CMD_WRITE_DMA_EX = 0x35;

// Port interrupt events
const uint32_t INT_CPDS = 0x1 << 31;
const uint32_t INT_TFES = 0x1 << 30;
const uint32_t INT_HBFS = 0x1 << 29;
const uint32_t INT_HBDS = 0x1 << 28;
const uint32_t INT_IFS = 0x1 << 27;
const uint32_t INT_INFS = 0x1 << 26;
const uint32_t INT_OFS = 0x1 << 24;
const uint32_t INT_IPMS = 0x1 << 23;
const uint32_t INT_PRCS = 0x1 << 22;
const uint32_t INT_DIS = 0x1 << 7;
const uint32_t INT_PCS = 0x1 << 6;
const uint32_t INT_DPS = 0x1 << 5;
const uint32_t INT_UFS = 0x1 << 4;
const uint32_t INT_SDBS = 0x1 << 3;
const uint32_t INT_DSS = 0x1 << 2;
const uint32_t INT_PSS = 0x1 << 1;
const uint32_t INT_DHRS = 0x1 << 0;

// Receive offsets
const uint8_t OFFSET_DFIS = 0x00;
const uint8_t OFFSET_PSFIS = 0x20;
const uint8_t OFFSET_RFIS = 0x40;
const uint8_t OFFSET_UFIS = 0x60;

// Port commands
const uint32_t PCMD_ICC = 0xf << 28;
const uint32_t PCMD_ASP = 0x1 << 27;
const uint32_t PCMD_ALPE = 0x1 << 26;
const uint32_t PCMD_DLAE = 0x1 << 25;
const uint32_t PCMD_ATAPI = 0x1 << 24;
const uint32_t PCMD_CPD = 0x1 << 20;
const uint32_t PCMD_ISP = 0x1 << 19;
const uint32_t PCMD_HPCP = 0x1 << 18;
const uint32_t PCMD_PMA = 0x1 << 17;
const uint32_t PCMD_CPS = 0x1 << 16;
const uint32_t PCMD_CR = 0x1 << 15;
const uint32_t PCMD_FR = 0x1 << 14;
const uint32_t PCMD_ISS = 0x1 << 13;
const uint32_t PCMD_CCS = 0x1f << 8;
const uint32_t PCMD_FRE = 0x1 << 4;
const uint32_t PCMD_CLO = 0x1 << 3;
const uint32_t PCMD_POD = 0x1 << 2;
const uint32_t PCMD_SUD = 0x1 << 1;
const uint32_t PCMD_ST = 0x1 << 0;

// Global HBA Control flags
const uint32_t GHC_AE = 0x1 << 31;
const uint32_t GHC_IE = 0x1 << 1;
const uint32_t GHC_HR = 0x1 << 0;

enum class FisType : uint8_t {
  reg_h2d = 0x27,   // Register FIS - host to device
  reg_d2h = 0x34,   // Register FIS - device to host
  dma_act = 0x39,   // DMA activate FIS - device to host
  dma_setup = 0x41, // DMA setup FIS - bidirectional
  data = 0x46,      // Data FIS - bidirectional
  bist = 0x58,      // BIST activate FIS - bidirectional
  pio_setup = 0x5f, // PIO setup FIS - device to host
  dev_bits = 0xa1,  // Set device bits FIS - device to host
};

struct FisRegH2d {
  // DWORD 0
  FisType fis_type; // FIS_TYPE_REG_H2D

  uint8_t pmport : 4; // Port multiplier
  uint8_t rsv0 : 3;   // Reserved
  uint8_t c : 1;      // 1: Command, 0: Control

  uint8_t command;  // Command register
  uint8_t featurel; // Feature register, 7:0

  // DWORD 1
  uint8_t lba0;   // LBA low register, 7:0
  uint8_t lba1;   // LBA mid register, 15:8
  uint8_t lba2;   // LBA high register, 23:16
  uint8_t device; // Device register

  // DWORD 2
  uint8_t lba3;     // LBA register, 31:24
  uint8_t lba4;     // LBA register, 39:32
  uint8_t lba5;     // LBA register, 47:40
  uint8_t featureh; // Feature register, 15:8

  // DWORD 3
  uint8_t countl;  // Count register, 7:0
  uint8_t counth;  // Count register, 15:8
  uint8_t icc;     // Isochronous command completion
  uint8_t control; // Control register

  // DWORD 4
  uint8_t rsv1[4]; // Reserved
};

struct FisRegD2h {
  // DWORD 0
  uint8_t fis_type; // FIS_TYPE_REG_D2H

  uint8_t pmport : 4; // Port multiplier
  uint8_t rsv0 : 2;   // Reserved
  uint8_t i : 1;      // Interrupt bit
  uint8_t rsv1 : 1;   // Reserved

  uint8_t status; // Status register
  uint8_t error;  // Error register

  // DWORD 1
  uint8_t lba0;   // LBA low register, 7:0
  uint8_t lba1;   // LBA mid register, 15:8
  uint8_t lba2;   // LBA high register, 23:16
  uint8_t device; // Device register

  // DWORD 2
  uint8_t lba3; // LBA register, 31:24
  uint8_t lba4; // LBA register, 39:32
  uint8_t lba5; // LBA register, 47:40
  uint8_t rsv2; // Reserved

  // DWORD 3
  uint8_t countl;  // Count register, 7:0
  uint8_t counth;  // Count register, 15:8
  uint8_t rsv3[2]; // Reserved

  // DWORD 4
  uint8_t rsv4[4]; // Reserved
};

struct PrdtEntry {
  uint32_t dba;  // Data base address
  uint32_t dbau; // Data base address upper 32 bits
  uint32_t rsv0; // Reserved

  // DW3
  uint32_t dbc : 22; // Byte count, 4M max
  uint32_t rsv1 : 9; // Reserved
  uint32_t i : 1;    // Interrupt on completion
};


class CommandTableBuilder {
  page::Page *page;
  volatile void *command_table;
  FisType type;
  volatile PrdtEntry *curr_prdt;

  const uint8_t CFIS_BASE = 0x00;
  const uint8_t ACMD_BASE = 0x40;
  const uint8_t PRDT_BASE = 0x80;

public:
  CommandTableBuilder() = delete;
  CommandTableBuilder(FisType type) : type(type) {
    page = page::alloc(page::FLAG_KERNEL, 1);
    command_table = page::kinstall(page, page::PAGE_TABLE_CACHEDISABLE | page::PAGE_TABLE_RW);
    memset_v(command_table, 0, PAGE_SIZE);
    curr_prdt = reinterpret_cast<volatile PrdtEntry *>(reinterpret_cast<uintptr_t>(command_table) + PRDT_BASE);
  }

  void set_h2d(uint8_t command, uint64_t lba, uint16_t sectors) {
    assert(type == FisType::reg_h2d);
    assert(page);
    volatile FisRegH2d *cmdfis = reinterpret_cast<volatile FisRegH2d *>(command_table);

    cmdfis->fis_type = FisType::reg_h2d;
    cmdfis->pmport = 0;
    cmdfis->rsv0 = 0;
    cmdfis->c = 1;
    cmdfis->command = command;
    cmdfis->featurel = 0;
    cmdfis->device = 1 << 6;
    cmdfis->featureh = 0;
    cmdfis->icc = 0;
    cmdfis->control = 0;
    cmdfis->rsv1[0] = 0;
    cmdfis->rsv1[1] = 0;
    cmdfis->rsv1[2] = 0;
    cmdfis->rsv1[3] = 0;

    cmdfis->lba0 = lba >> 0;
    cmdfis->lba1 = lba >> 8;
    cmdfis->lba2 = lba >> 16;
    cmdfis->lba3 = lba >> 24;
    cmdfis->lba4 = lba >> 32;
    cmdfis->lba5 = lba >> 40;

    cmdfis->countl = sectors;
    cmdfis->counth = sectors >> 8;
  }

  uint16_t populate_prdt(page::Page *page, uint32_t bytes, bool interrupt) {
    assert(BYTES_PER_PRDT <= 1024 * 1024 * 4);
    assert(page);
    uint16_t entries = 0;
    addr_phys_t host = page->get_mem_base();

    while (bytes) {
      entries++;
      curr_prdt->i = interrupt;
      curr_prdt->dba = host;
#if X86_64
      curr_prdt->dbau = host >> 32;
#endif

      size_t to_copy = BYTES_PER_PRDT;

      if (to_copy > bytes) {
        to_copy = bytes;
      }
      if (host + to_copy > page->get_mem_limit()) {
        to_copy = page->get_mem_limit() - host;
        page = page->next;
        assert(page);
      }

      curr_prdt->dbc = to_copy - 1;

      bytes -= to_copy;
      host += to_copy;
      curr_prdt++;
    }
    return entries;
  }

  page::Page *get() {
    page::Page *ret = page;
    page::kuninstall(command_table, ret);
    page = nullptr;
    return ret;
  }
};

enum class Sig : uint32_t {
  ata = 0x00000101,
  atapi = 0xeb140101,
  semb = 0xc33c0101,
  pm = 0x96690101 // Port multiplier
};

enum class IpmState : uint8_t {
  not_established = 0x0,
  active = 0x1,
  partial = 0x2,
  slumber = 0x6,
  dev_sleep = 0x8,
};

enum class InterfaceSpeed : uint8_t { not_established = 0x0, gen1 = 0x1, gen2 = 0x2, gen3 = 0x3 };

enum class DetectionStatus : uint8_t {
  not_present = 0x0,
  not_established = 0x1,
  present_and_established = 0x3,
  offline = 0x4
};

struct CommandHeader {
  // DW0
  uint8_t cfl : 5; // Command FIS length in DWORDS, 2 ~ 16
  uint8_t a : 1;   // ATAPI
  uint8_t w : 1;   // Write, 1: H2D, 0: D2H
  uint8_t p : 1;   // Prefetchable

  uint8_t r : 1;    // Reset
  uint8_t b : 1;    // BIST
  uint8_t c : 1;    // Clear busy upon R_OK
  uint8_t rsv0 : 1; // Reserved
  uint8_t pmp : 4;  // Port multiplier port

  uint16_t prdtl; // Physical region descriptor table length in entries

  // DW1
  volatile uint32_t prdbc; // Physical region descriptor byte count transferred

  // DW2, 3
  uint32_t ctba;  // Command table descriptor base address
  uint32_t ctbau; // Command table descriptor base address upper 32 bits

  // DW4 - 7
  uint32_t rsv1[4]; // Reserved
};

const uint8_t AHCI_CLASS = 0x01;
const uint8_t AHCI_SUBCLASS = 0x06;

struct Port {
  uint32_t clb;       // 0x00, command list base address, 1K-byte aligned
  uint32_t clbu;      // 0x04, command list base address upper 32 bits
  uint32_t fb;        // 0x08, FIS base address, 256-byte aligned
  uint32_t fbu;       // 0x0C, FIS base address upper 32 bits
  uint32_t is;        // 0x10, interrupt status
  uint32_t ie;        // 0x14, interrupt enable
  uint32_t cmd;       // 0x18, command and status
  uint32_t rsv0;      // 0x1C, Reserved
  uint32_t tfd;       // 0x20, task file data
  uint32_t sig;       // 0x24, signature
  uint32_t ssts;      // 0x28, SATA status (SCR0:SStatus)
  uint32_t sctl;      // 0x2C, SATA control (SCR2:SControl)
  uint32_t serr;      // 0x30, SATA error (SCR1:SError)
  uint32_t sact;      // 0x34, SATA active (SCR3:SActive)
  uint32_t ci;        // 0x38, command issue
  uint32_t sntf;      // 0x3C, SATA notification (SCR4:SNotification)
  uint32_t fbs;       // 0x40, FIS-based switch control
  uint32_t rsv1[11];  // 0x44 ~ 0x6F, Reserved
  uint32_t vendor[4]; // 0x70 ~ 0x7F, vendor specific
};

class AhciDevice {
private:
  volatile Port *port;
  uint8_t port_offset;
  uint8_t command_slots;
  page::Page *page;
  Buffer<volatile CommandHeader> clb_data;
  Buffer<volatile uint32_t> received_fis;

  volatile FisRegD2h *d2h_header;

  class AhciDeviceWorker : public storage_worker::Worker {
  public:
    AhciDevice *device;
    size_t orders[32];
    page::Page *command_tables[32];

    virtual bool dev_handle(storage_worker::WorkOrder *wo) {
      uint8_t slot = device->find_slot();
      if (slot == 0xff) {
        return false;
      }
      volatile CommandHeader &header = device->clb_data[slot];

      page::Page *alloc = wo->host.get_page();
      assert(alloc);

      CommandTableBuilder ctb(FisType::reg_h2d);
      uint8_t cmd = wo->write ? ATA_CMD_WRITE_DMA_EX : ATA_CMD_READ_DMA_EX;
      ctb.set_h2d(cmd, wo->device / SECTOR_SIZE, wo->size / SECTOR_SIZE);
      uint16_t entries = ctb.populate_prdt(alloc, wo->size, true);
      page::Page *command_table = ctb.get();

      header.cfl = sizeof(FisRegH2d) / sizeof(uint32_t);
      header.a = 0;
      header.w = wo->write;
      header.p = 0;
      header.r = 0;
      header.b = 0;
      header.c = 1;
      header.pmp = 0;
      header.prdtl = entries;
      header.prdbc = 0;
      header.ctba = command_table->get_mem_base();
      header.ctbau = 0;

      orders[slot] = wo->slot;
      command_tables[slot] = command_table;
      device->port->ci |= 1 << slot;

      return true;
    }

    void check_completion() {
      uint32_t slots = device->port->ci;
      bool cleared = false;

      for (uint32_t i = 0; i < device->command_slots; i++) {
        if ((slots & 1) == 0 && orders[i] != UINT32_MAX) {
          storage_worker::complete_work(orders[i]);
          orders[i] = UINT32_MAX;
          page::free(command_tables[i]);
          cleared = true;
        }
        slots >>= 1;
      }

      if (cleared) {
        need_update = true;
        storage_worker::update();
      }
    }

    void clear_slots() {
      for (auto i = 0; i < 32; i++) {
        orders[i] = UINT32_MAX;
      }
    }
  };
  AhciDeviceWorker worker;

public:
  AhciDevice(volatile Port *port, uint8_t offset, uint8_t command_slots)
      : port(port), port_offset(offset), command_slots(command_slots) {}

  void configure() {
    page = page::alloc(page::FLAG_KERNEL, 1);

    // Place the CLB data
    port->cmd &= ~(PCMD_FRE | PCMD_ST);
    port->clb = page->get_mem_base();
    void *install_point = page::kinstall(page, page::PAGE_TABLE_CACHEDISABLE | page::PAGE_TABLE_RW);
    clb_data = Buffer<volatile CommandHeader>(install_point, 32);

    // Place the recieved FIS
    port->fb = page->get_mem_base() + 0x400;
    received_fis = Buffer<volatile uint32_t>(reinterpret_cast<volatile uint32_t *>(clb_data.end()), 0x100 / 4);
    d2h_header = reinterpret_cast<volatile FisRegD2h *>(&received_fis[OFFSET_RFIS / sizeof(volatile uint32_t)]);
    port->cmd |= PCMD_FRE;

    // Ensure the port is connected
    if (!((port->ssts >> 8) & 0x7)) {
#if DEBUG_AHCI
      printk("[AHCI] Port %x is not connected.\n", port_offset);
#endif
      return;
    }
#if DEBUG_AHCI
    printk("[AHCI] Port %x is connected.\n", port_offset);
#endif

    // Create a worker for this device
    worker.device = this;
    worker.clear_slots();
    worker.id = storage_worker::generate_worker_id();
    storage_worker::add_worker(&worker);

    // Enable the device
    port->ie = 0xffffffff;
    port->cmd |= PCMD_ST;
  }

  void handle_interrupt() {
    uint32_t is = port->is;

    // Consume a specific interrupt flag and remove it from the status
    auto consume = [&](uint32_t flag) {
      if (is & flag) {
        is &= ~flag;
        return true;
      }
      return false;
    };

    // Task File Error
    if (consume(INT_TFES)) {
      // TODO: Look at this
      printk("Task file error set: %x\n", port->tfd);
    }

    // Recieved D2H packet
    if (consume(INT_DHRS)) {
      // TODO: Look at this
    }

    worker.check_completion();

    if (is) {
      panic("[AHCI] Could not consume all interrupts (remaining 0x%x)\n", is);
    }

    // Clear all interrupts
    port->is = 0xffffffff;
  }

  IpmState get_ipm_state() const { return static_cast<IpmState>((port->ssts >> 8) & 0xf); }

  InterfaceSpeed get_interface_speed() const { return static_cast<InterfaceSpeed>((port->ssts >> 4) & 0xf); }

  DetectionStatus get_detection() const { return static_cast<DetectionStatus>(port->ssts & 0xf); }

  uint8_t find_slot() const volatile {
    // If not set in SACT and CI, the slot is free
    uint32_t slots = (port->sact | port->ci);
    for (uint32_t i = 0; i < command_slots; i++) {
      if ((slots & 1) == 0)
        return i;
      slots >>= 1;
    }
    return 0xff;
  }
};

class AhciDriver : public Driver {
private:
  unique_ptr<AhciDevice> devices[32] = {};

  struct hba_t {
    // 0x00 - 0x2B, Generic Host Control
    uint32_t cap;     // 0x00, Host capability
    uint32_t ghc;     // 0x04, Global host control
    uint32_t is;      // 0x08, Interrupt status
    uint32_t pi;      // 0x0C, Port implemented
    uint32_t vs;      // 0x10, Version
    uint32_t ccc_ctl; // 0x14, Command completion coalescing control
    uint32_t ccc_pts; // 0x18, Command completion coalescing ports
    uint32_t em_loc;  // 0x1C, Enclosure management location
    uint32_t em_ctl;  // 0x20, Enclosure management control
    uint32_t cap2;    // 0x24, Host capabilities extended
    uint32_t bohc;    // 0x28, BIOS/OS handoff control and status

    // 0x2C - 0x9F, Reserved
    uint8_t rsv[0xA0 - 0x2C];

    // 0xA0 - 0xFF, Vendor specific registers
    uint8_t vendor[0x100 - 0xA0];

    // 0x100 - 0x10FF, Port control registers
    Port ports[32]; // 1 ~ 32
  };

  volatile hba_t *hba;
  uint8_t command_slots;

public:
  AhciDriver(Device &device) : Driver(device){};

  virtual void configure_post_int() override {
    // TODO: What happens if the hba crosses over page boundries
    addr_phys_t bar5 = (addr_phys_t)device.get32(0, BAR5) & ~0xf;
    addr_phys_t bar5_offset = bar5 % PAGE_SIZE;
    addr_phys_t bar5_base = bar5 - bar5_offset;
    page::Page *page = page::create(bar5_base, page::FLAG_KERNEL, 1);
    hba = (volatile hba_t *)((addr_logical_t)page::kinstall(page, page::PAGE_TABLE_CACHEDISABLE | page::PAGE_TABLE_RW) +
                             bar5_offset);
    command_slots = ((hba->cap >> 8) & 0x1f) + 1;

    // Reset the device
    hba->ghc |= GHC_HR;
    while (hba->ghc & GHC_HR)
      ;
    hba->ghc |= GHC_IE | GHC_AE;

    // Identify all the devices
    for (int i = 0; i < 32; i++) {
      if ((hba->pi >> i) & 0x1) {
        devices[i] = make_unique<AhciDevice>(&(hba->ports[i]), i, command_slots);
        devices[i]->configure();
      }
    }
  }

  virtual void handle_interrupt() override {
    uint32_t is = hba->is;
    uint8_t port = 0;
    while (is) {
      if (is & 0x1) {
        devices[port]->handle_interrupt();
      }
      port++;
      is >>= 1;
    }
    hba->is = 0xffffffff;
  }

  virtual Utf8 device_name() override {
    return Utf8("Ahci Bus (Version %i.%i)").format(major_version(), minor_version());
  }

  virtual Utf8 driver_name() override { return Utf8("AhciDriver"); }

  uint8_t major_version() const { return hba->vs >> 16; }

  uint8_t minor_version() const { return ((hba->vs >> 4) & 0xf) + (hba->vs & 0xf); }

  virtual ~AhciDriver(){};
};

class AhciDriverFactory : public DriverFactory {
public:
  virtual void create_driver(Device &device) override { device.driver = make_unique<AhciDriver>(device); }

  virtual void search_for_device(const vector<Device> &devices) override {
    for (Device &d : devices) {
      if (d.device_class == AHCI_CLASS && d.device_subclass == AHCI_SUBCLASS) {
        create_driver(d);
      }
    }
  }
};

RegisterDriverFactory<AhciDriverFactory> df;
} // namespace
