#include <stdint.h>

#include "hw/acpi.hpp"
#include "hw/pci/pci.hpp"
#include "int/ioapic.hpp"
#include "int/lapic.hpp"
#include "logging/printk.hpp"
#include "main/panic.hpp"
#include "structures/list.hpp"
#include "structures/mutex.hpp"

extern "C" {
#include "hw/ports.h"
#include "hw/utils.h"
#include "int/numbers.h"
}

namespace pci {

namespace {
void interrupt(idt_proc_state_t state, addr_logical_t ret) {
  // TODO: Only search for specific devices
  for (auto &d : devices) {
    if (d.driver) {
      d.driver->handle_interrupt();
    }
  }
  lapic::eoi();
}
vector<uint32_t> registered_irqs;
} // namespace

vector<Device> devices;
vector<unique_ptr<DriverFactory>> &getDriverFactoryRegistry() {
  static vector<unique_ptr<DriverFactory>> driverFactoryRegistry;
  return driverFactoryRegistry;
}

uint8_t read8(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
  uint32_t addr;

  addr = (bus << 16) | (slot << 11) | (func << 8) | offset | 0x80000000;

  outl(IO_PORT_PCI_ADDRESS, addr);

  uint8_t input = inb(IO_PORT_PCI_DATA);
  return input;
}

uint32_t read(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
  uint32_t addr;

  if (offset % 0x4) {
    panic("Unaligned access in PCI bus.");
  }

  addr = (bus << 16) | (slot << 11) | (func << 8) | offset | 0x80000000;

  outl(IO_PORT_PCI_ADDRESS, addr);

  return inl(IO_PORT_PCI_DATA);
}

void write8(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint8_t val) {
  uint32_t addr;

  addr = (bus << 16) | (slot << 11) | (func << 8) | offset | 0x80000000;

  outl(IO_PORT_PCI_ADDRESS, addr);
  outb(IO_PORT_PCI_DATA, val);
}

void write(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint32_t val) {
  uint32_t addr;

  if (offset % 0x4) {
    panic("Unaligned access in PCI bus.");
  }

  addr = (bus << 16) | (slot << 11) | (func << 8) | offset | 0x80000000;

  outl(IO_PORT_PCI_ADDRESS, addr);
  outl(IO_PORT_PCI_DATA, val);
}

static uint32_t read_device_vendor(uint8_t bus, uint8_t slot, uint8_t fn) { return read(bus, slot, fn, 0); }


Device::Device(uint8_t bus, uint8_t slot) : bus(bus), slot(slot) {
  uint32_t first = this->get32(0, VENDOR_ID);
  this->vendor_id = first & 0xffff;
  this->device_id = first >> 16;

  uint32_t second = this->get32(0, REVISION);
  this->revision = second & 0xff;
  this->prog_if = (second >> 8) & 0xff;
  this->device_subclass = (second >> 16) & 0xff;
  this->device_class = (second >> 24) & 0xff;

  uint8_t ht = this->get8(0, HEADER_TYPE);
  this->header_type = ht & 0x7f;
  this->multifunction = ht & 0x80;
}

uint8_t Device::get8(uint8_t fn, uint8_t addr) const {
  uint8_t base = addr & 0xfc;
  uint8_t offset = addr % 0x4;

  uint32_t result = this->get32(fn, base);
  return (result >> offset * 8) & 0xff;
}

uint16_t Device::get16(uint8_t fn, uint8_t addr) const {
  uint8_t base = addr & 0xfc;
  uint8_t offset = addr % 0x4;

  uint32_t result = this->get32(fn, base);
  return (result >> offset * 8) & 0xffff;
}

uint32_t Device::get32(uint8_t fn, uint8_t addr) const { return read(this->bus, this->slot, fn, addr); }

uint16_t Device::get_status(uint8_t fn, uint16_t flag) const { return get16(fn, STATUS) & flag; }

uint8_t Device::find_capabilities_offset(uint8_t fn, uint8_t id) {
  uint8_t cap_offset = get8(fn, CAPABILITIES);
  while (cap_offset) {
    uint32_t hdr = get32(fn, cap_offset);
    uint8_t this_id = hdr & 0xff;
    if (this_id == id) {
      return cap_offset;
    }
    cap_offset = (hdr >> 8) & 0xff;
  }
  return 0;
}

namespace {
void search_bus(uint8_t bus, uint8_t start) {
  for (uint16_t i = start; i < 32; i++) {
    if (read_device_vendor(bus, i, 0) != 0xffffffff) {
      Device &device = (devices.emplace_back(bus, i), devices.back());

      // Check if it is a pci to pci bridge
      if (device.device_class == 0x06 && device.device_subclass == 0x04) {
#if DEBUG_PCI
        printk("Found PCI bridge!\n");
#endif
        search_bus(device.get8(0, PBR_SECONDARY_BUS_NUM), 0);
      }

      // Search for other functions
      if (device.multifunction) {
        if (read_device_vendor(bus, i, 0) != 0xffffffff) {
          // Check THIS for being a PCI to PCI bus
          if (device.device_class == 0x06 && device.device_subclass == 0x04) {
#if DEBUG_PCI
            printk("Found PCI bridge!\n");
#endif
            search_bus(device.get8(0, PBR_SECONDARY_BUS_NUM), 0);
          }
        }
      }
    }
  }
}
} // namespace

void init() {
  Device &device = (devices.emplace_back(0, 0), devices.back());

  // Seach devices
  if (device.multifunction) {
    for (uint8_t fn = 0; fn < 8; fn++) {
      if (read_device_vendor(0, 0, fn) != 0xffffffff)
        break;
      search_bus(fn, fn == 0 ? 1 : 0);
    }
  } else {
    search_bus(0, 1);
  }

  // And create drivers for them
  for (unique_ptr<DriverFactory> &df : getDriverFactoryRegistry()) {
    df->search_for_device(devices);
  }

  // And then configure them
  for (Device &d : devices) {
    if (d.driver) {
      d.driver->configure();
    }
  }

  // Set up the ACPI information
  acpi::init_pci();

  // And then do post-interrupt configuration
  for (Device &d : devices) {
    if (d.driver) {
      d.driver->configure_post_int();
    }
  }
}

void print_devices() {
  for (const auto &device : devices) {
    if (device.driver) {
      printk("[%x:%x] %s - %s (%x:%x, %x:%x.%x)\n", device.bus, device.slot, device.driver->device_name().to_string(),
             device.driver->driver_name().to_string(), device.vendor_id, device.device_id, device.device_class,
             device.device_subclass, device.prog_if);
    } else {
      printk("[%x:%x] (unknown) (%x:%x - %x:%x.%x)\n", device.bus, device.slot, device.vendor_id, device.device_id,
             device.device_class, device.device_subclass, device.prog_if);
    }
  }
}

void tell_irq(uint32_t irq) {
  for (auto known : registered_irqs) {
    if (known == irq)
      return;
  }

#if DEBUG_PCI
  printk("PCI using IRQ %x (%x)\n", irq, acpi::get_irq_override(irq));
#endif
  registered_irqs.push_back(irq);
  ioapic::enable_func(acpi::get_irq_override(irq), interrupt, 0);
}
} // namespace pci
